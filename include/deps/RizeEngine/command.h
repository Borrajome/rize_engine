#ifndef COMMAND_H_
#define COMMAND_H_ 1

#include "platform_types.h"

namespace RZE {
  class Command {
  public:
    Command() {};
    ~Command() {};

	virtual void execute() = 0;
  };
}
#endif // COMMAND_H_