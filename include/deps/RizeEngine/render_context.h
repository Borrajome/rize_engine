#ifndef RENDER_CONTEXT_H_
#define RENDER_CONTEXT_H_ 1

#include "internalbuffer.h"
#include"internal_material.h"
#include "internal_texture.h"
#include "node.h"
#include "display_list.h"
#include "obj_data_structs.h"
#include "mat4x4.hpp"

#include <mutex>
#include <condition_variable>

#include <thread>
#include <chrono>


namespace RZE {

	class RenderContext {
	public:
		RenderContext();
		~RenderContext();

		int32 copyDisplayList(DisplayList *display_list);
		void render();

		uint32 createBuffer(uint32 size);
		uint32 createMaterial();

		InternalBuffer* getInternalBuffer(uint32 position);
		InternalMaterial* getInternalMaterial(uint32 position);
		InternalTexture* getInternalTexture(uint32 position);

	  int32 updateViewMatrix(glm::mat4 view_matrix);

		InternalNodes& internal_nodes();
		void initNode(Node *node);

    void CalculateLocalModels();
    void CalculateNestedModels();
    void RenderNodes(DisplayList *dl);

    uint32 get_vertex_shader_id(uint32 id);
    uint32 get_fragment_shader_id(uint32 id);

		glm::mat4 viewMatrix();


    uint32 material_ids_[3];

	private:

    void RecursiveModelCalculation(uint32 node_id);

		DisplayList *current_displayList;
		

		InternalBuffer * internal_buffer_;
		InternalMaterial *internal_materials_;
		InternalTexture *internal_textures_;
		InternalNodes internal_nodes_;


		uint32 buffers_size_;
		uint32 materials_size_;
		uint32 textures_size_;

		uint32 vertex_shader_ids_[3];
		uint32 fragment_shader_ids_[3];
    uint32 current_vertex_shader_id_;
    uint32 current_fragment_shader_id_;


		glm::mat4 view_matrix_;

		std::mutex m_pop;
		std::condition_variable cv_pop;

		std::mutex m_push;
		std::condition_variable cv_push;

		bool ready = true;
		
	};

}
#endif