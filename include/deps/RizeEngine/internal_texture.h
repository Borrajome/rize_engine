#ifndef INTERNAL_TEXTURE_H
#define INTERNAL_TEXTURE_H 1


#include "platform_types.h"
#include "deps/GLEW/glew.h"


namespace RZE {

	class InternalTexture {
	public:

		InternalTexture();
		~InternalTexture();

		void reset();

		void setGPUID(uint32 GPUID);
		void setInit(bool init);
		void setRender(bool render);

		uint32 GPUID();
		bool hasInitialized();
		bool hasRendered();

		void setTextureType(GLenum type);
		void setTextureUnit(uint32 text_unit);

		uint32 textureUnit();
		GLenum textureType();

	private:
		uint32 GPUID_;
		bool render_;
		bool init_;
		GLenum text_type_;
		uint32 text_unit_;
	};


}

#endif //INTERNAL_TEXTURE_H