#ifndef COMMAND_CLEAR_WINDOW_H_
#define COMMAND_CLEAR_WINDOW_H_ 1

#include "command.h"

namespace RZE {

  enum ClearBit {
     kClearBit_Color = 1,
     kClearBit_Depth = 2,
     kClearBit_Stencil = 4,
     kClearBit_Accum = 8,
  };

  class Command_ClearWindow : public Command {
  public:

    Command_ClearWindow();
    ~Command_ClearWindow();

    void init(float32 color[4], int32 mask_);
    void execute() override;

  private:

    float32 color_[4];
    int32 bit_mask_;

  };
}


#endif //COMMAND_CLEAR_WINDOW_H_