#ifndef COMMAND_INDEX_BUFFER_H_
#define COMMAND_INDEX_BUFFER_H_ 1

#include "command.h"
#include <vector>

namespace RZE {

  class RenderContext;

	class Command_IndexBuffer : public Command {

	public:
		Command_IndexBuffer();
		~Command_IndexBuffer();

		void init(RenderContext *ctx, ushort16 *index, uint32 array_size, uint32 index_buffer_int);

		void execute()override;

	private:

		ushort16 *indices_;
		uint32 array_size_;
		uint32 index_buffer_int_;

    RenderContext *ctx_;
	};

}
#endif //COMMAND_INDEX_BUFFER_H_

