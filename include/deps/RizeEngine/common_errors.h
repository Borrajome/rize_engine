#ifndef COMMON_ERRORS_H_
#define COMMON_ERRORS_H_ 1

#include <stdio.h>
#include <cstdio>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/async.h"


namespace RZE {

  
  enum ErrorCode {
    kErrorCode_Ok = 0,
    kErrorCode_NullParam,
    kErrorCode_MemoryAlloc,
    kErrorCode_NullArrayPos,
  };


  static std::shared_ptr<spdlog::logger> logger_;
  struct Error {
    ErrorCode error_;
    void logError() {
      switch (error_) {
        case 0: logger_->debug("Ok"); break;
        case 1: logger_->error("NullParameter"); break;
        case 2: logger_->error("MemoryAllocation"); break;
        case 3: logger_->error("NullArrayPosition"); break;
        default: logger_->debug("Unregistered ErrorCode: {}", error_); break;
      }
    }
    void logError(ErrorCode error) {
      switch (error) {
      case 0: logger_->debug("Ok"); break;
      case 1: logger_->error("NullParameter"); break;
      case 2: logger_->error("MemoryAllocation"); break;
      case 3: logger_->error("NullArrayPosition"); break;
      default: logger_->debug("Unregistered ErrorCode: {}", error_); break;
      }
    }
  };
}


#endif // COMMON_ERRORS_H_