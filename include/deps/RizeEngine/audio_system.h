// ----------------------------------------------------------------------------
// Copyright (C) Rize Engine / Mario Borrajo Megoya / Jorge Virgos Castejon
// Copyright (C) 2018 Mario Borrajo Megoya 
// Audio Management Class.
// ---------------------------------------------------------------------------

#ifndef AUDIOSYSTEM_CLASS_H_
#define AUDIOSYSTEM_CLASS_H_1

#include "deps/FMOD/lowlevel/fmod.hpp"
#include "deps/FMOD/studio/fmod_studio.hpp"

#include "deps/SoLoud/soloud.h"

#include <cstdlib>
#include <string>
#include <stdio.h>
#include <vector>
#include <map>
#include <math.h>

namespace RZE {

  class AudioLoader;

#ifdef FMOD_AUDIO

/// @brief Structure track that manages the sound files that we will reproduce
struct Track {
	FMOD::Sound *sound_ = nullptr; ///< sound_ pointer that contains the file info to reproduce
	FMOD::Channel *channel_ = nullptr; ///< channel_ the channel of the track in wich one the sounds we will play
};

/// @brief Structure that contains the Track to reproduce and its time to make its delay play
struct Layering {

	/// @brief inits the track pointer and set the delay time
	/// @params track track pointer to the track to reproduce
	/// @params delay_time the amount of time to make the delay
	void init(Track *track, float delay_time);

	float delay_time_; ///< the amount of time to make the delay
	uint64_t clock_; ///< the number of samples per second in the software mixer.
	int rate_; ///< output format for the software mixer.
	Track *track_ = nullptr; ///< track pointer to the track to reproduce
};


/// @brief Class that manages the audio using FMOD library
class AudioSystem {
public:

	/// @brief Default constructor
	AudioSystem();
	/// @brief Default destructor
	~AudioSystem();

	/// @brief updates all the FMOD objects
	void update();

	/// @brief Closes the FMOD System and release the Tracks info
	void shutdown();

	/// @brief Charges in the Track a sound from disc
	/// @param track corresponds to the track in wich one charge the sound
	/// @param fileName the path of the sound file
	/// @param loop bool to manage the loop of the sound
	void chargeSound(Track *track, const char *fileName, bool loop);

	/// @brief Plays the sound of a track in their channel
	/// @param track the track from we reproduce the sound
	/// @param volume float that corresponds the value of volume for the sound
	void playSound(Track *track, float volume);

	/// @brief Stops the channels from a track
	/// @param track the corresponds track to stop the channel
	void stopChannel(Track *track);

	/// @brief Stops all Channels
	void stopAllChannel();
	
	/// @brief Play a Channel sound from the beggining
	/// @param track 
	void playChannel(Track *track);

	/// @brief Sets the value of the volume from a channel
	/// @param track 
	/// @param volume the valu of the volume
	void setChannelVolume(Track *track, float volume);

	/// @brief Sets the loop of a channel
	/// @param track
	/// @param loop the state of the loop
	void setLooping(Track *track, bool loop);

	/// @brief Stops the branching
	void stopBranching();

	/// @brief Mades a Crossfade between two tracks
	/// @param track1 the first track
	/// @param track2 the second tranck
	/// @param out_seconds the seconds in wich the first track made the first fade out
	/// @param in_seconds the seconds in wich the second track mades the fade in
	/// @params delay the time to produce the croosfade
	void crossfading(Track *track1, Track *track2, int out_seconds,int in_seconds, int delay);
	
	/// @brief Mades the layering between sounds
	/// @param layer_vec the vector that contains all the sounds to layer
	void layering(std::vector<Layering>*layer_vec);

	/// @brief Mades branching between sounds
	/// @param track1 frist track
	/// @param track2 second track
	void branching(Track *next_track, Track *transition_track);

	/// @brief FMOD error checking
	int errorCheck(FMOD_RESULT result);

	
  AudioLoader *AudioLoader;
	Track Tracks[16]; ///< Array of posible Tracks

private:
	FMOD::System *system_; ///< FMOD system
	std::vector<Track*> playlist_; ///< vector of Tracks for branching
};

#else

/// @brief Structure track that manages the sound files that we will reproduce
struct Track {
	SoLoud::AudioSource *sound_ = nullptr; ///< Instance oof an AudioSource
	SoLoud::handle channel_ = 0; // It's just an unsigned int identifier
};

/// @brief Structure that contains the Track to reproduce and its time to make its delay play
struct Layering {

	/// @brief inits the track pointer and set the delay time
	/// @params track track pointer to the track to reproduce
	/// @params delay_time the amount of time to make the delay
	void init(Track *track, float delay_time);

	float delay_time_; ///< the amount of time to make the delay
	Track *track_ = nullptr; ///< track pointer to the track to reproduce
};

/// @brief Class that manages the audio using Soloud library
class AudioSystem {
public:

	/// @brief Default constructor
	AudioSystem();

	/// @brief Default destructor
	~AudioSystem();

	/// @brief updates all the Soloud objects
	void update();

	/// @brief Free the tracks' memory and destroy audiosystem itself, if needed
	void shutdown();

	/// @brief Charges in the Track a sound from disc
	/// @param track corresponds to the track in wich one charge the sound
	/// @param fileName the path of the sound file
	/// @param loop bool to manage the loop of the sound
	void chargeSound(Track *track, const char *fileName, bool loop);

	/// @brief Plays the sound of a track in their channel
	/// @param track the track from we reproduce the sound
	/// @param volume float that corresponds the value of volume for the sound
	void playSound(Track *track, float volume);

	/// @brief Stops the channels from a track
	/// @param track the corresponds track to stop the channel
	void stopChannel(Track *track);

	/// @brief Stops all Channels
	void stopAllChannel();

	/// @brief Play a Channel sound from the beggining
	/// @param track 
	void playChannel(Track *track);

	/// @brief Sets the value of the volume from a channel
	/// @param track 
	/// @param volume the valu of the volume
	void setChannelVolume(Track *track, float volume);

	/// @brief Mades a Crossfade between two tracks
	/// @param track1 the first track
	/// @param track2 the second tranck
	/// @param out_seconds the seconds in wich the first track made the first fade out
	/// @param in_seconds the seconds in wich the second track mades the fade in
	/// @params delay the time to produce the croosfade
	void crossfading(Track *track1, Track *track2, int out_seconds, int in_seconds, int delay);
	
	/// @brief Mades the layering between sounds
	/// @param layer_vec the vector that contains all the sounds to layer
	void layering(std::vector<Layering>*layer_vec);

	/// @brief Mades branching between sounds
	/// @param track1 frist track
	/// @param track2 second track
	void branching(Track *next_track, Track *transition_track);

	/// @brief Stops the branching
	void stopBranching();

	/// @brief FMOD error checking
	int errorCheck(SoLoud::result result);

	Track Tracks[16]; ///< Array of posible Tracks
  RZE::AudioLoader *AudioLoader;

private:
	SoLoud::Soloud *system_; ///< Soloud system
	std::vector<Track*> playlist_; ///< vector of Tracks for branching
};

#endif

}
#endif
