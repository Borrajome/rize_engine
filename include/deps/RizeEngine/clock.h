// ----------------------------------------------------------------------------
// Copyright (C) Rize Engine / Mario Borrajo Megoya / Jorge Virgos Castejon
// Copyright (C) 2018 
// Clock Management Class.
// ----------------------------------------------------------------------------

#ifndef CLOCK_CLASS_H_
#define CLOCK_CLASS_H_1

#include "windows.h"


namespace RZE {
	class Clock {
	public:

		Clock();
		~Clock();
		Clock(const Clock& other);

		double ellapsedMS();

		void start();

		void stop();


	private:
		LARGE_INTEGER startTime;
		LARGE_INTEGER endTime;

	};
}
#endif