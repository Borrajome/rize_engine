//#ifndef __APP_CLAS_H__
//#define __APP_CLAS_H__
//
//#include "deps/PX/px_sched.h"
//#include <thread>
//#include <mutex>
//#include <condition_variable>
//#include <stdio.h>
//#include <conio.h>
//#include <chrono>
//#include <vector>
//#include <string>
//#include <iostream>
//#include <time.h>
//#include "common_errors.h"
//
//#include "obj_loader.h"
//
//
//#include "platform_types.h"
//#include "render_context.h"
//#include "window.h"
//#include "glfw3.h"
//#include "imgui_window.h"
//#include "clock.h"
//#include "display_list.h"
//#include "camera.h"
//#include <gtc/matrix_transform.hpp>
//#include "minitrace/minitrace.h"
//
//namespace RZE {
//
//	class App {
//	public:
//		App();
//		~App();
//
//		void init();
//		void start();
//		void shutDown();
//
//		void inputService();
//		//No estoy seguro de si es esto o no?
//		//void gameUpdate(float deltaTime);
//		void logic_init();
//		void logic_update(/*float deltaTime*/);
//
//		void draw();
//
//	private:
//		bool quit_game_;
//		double time_step_;
//
//		float32 clear_color[4];
//		int32 screen_mask;
//
//		float32 movement;
//
//		RZE::Window mainWindow;
//		RZE::RenderContext *render_ctx;
//		RZE::Camera camera;
//
//		px_sched::Scheduler schd;
//		px_sched::SchedulerParams schd_params;
//		px_sched::Sync logic_init_sync_;
//		px_sched::Sync logic_update_sync_;
//
//		RZE::DisplayList *logic_dl;
//		RZE::RenderObjectIndexes State_;
//	};
//}
//
//#endif // __APP_CLAS_H__
