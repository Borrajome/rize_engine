#ifndef DISPLAY_LIST_H_
#define DISPLAY_LIST_H_ 1

#include <vector>
#include "obj_data_structs.h"
#include "command.h"
#include "command_clearWindow.h"
#include "command_createMaterial.h"
#include "command_createTexture.h"
#include "command_indexBuffer.h"
#include "command_clearWindow.h"
#include "command_render.h"
#include "command_vertexBuffer.h"
#include "mat4x4.hpp"


namespace RZE {

  class RenderContext;

	class DisplayList {
	public:

		DisplayList();
		DisplayList(const DisplayList &displaylist_obj);
		~DisplayList();


		int32 clearVector();

		int32 clearCommandList();

		int32 vertexBuffer(unsigned int vertex_buffer_int, float *positions, int p_size, float* normals, int n_size, float* uvs, int u_size);

		int32 indexBuffer(ushort16 *index, int32 array_size, uint32 index_buffer_int);

		int32 createMaterial(uint32 material, uint32 vertex_id, uint32 fragment_id);

		int32 createTexture(const char* filename, uint32 texture_id, Type type,
			Parameter parameter, Wrap wrap,
		    Format format,Filter filter);

		int32 clearWindow(float32 color[4], int32 mask);

		int32 render(uint32 program_int, uint32 vertex_int, uint32 index_int, uint32 node_id,uint32 texture_id);

		int32 executeCommands();

		void setRenderContext(RenderContext *ctx);

		std::vector<Command*> getCommandList();

	private:
		std::vector<Command*> command_list;

		RenderContext *current_ctx_;


	};
}

#endif // DISPLAY_LIST_H_