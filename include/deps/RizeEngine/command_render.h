#ifndef COMMAND_RENDER_H_
#define COMMAND_RENDER_H_ 1

#include "command.h"

namespace RZE {

  class RenderContext;
  
  class Command_Render : public Command {
  public:

    Command_Render();
    ~Command_Render();

    void init(RenderContext *ctx, uint32 program_int, uint32 vertex_int, uint32 index_int, uint32 node_id_, uint32 texture_id);
    void execute() override;

  private:
  
    uint32 program_int_;
    uint32 vertex_int_;
    uint32 index_int_;
    uint32 node_id_;
	uint32 texture_id_;

    RenderContext *ctx_;
  };
}


#endif //COMMAND_RENDER_H_