#ifndef COMMAND_VERTEX_BUFFER_H_
#define COMMAND_VERTEX_BUFFER_H_ 1

#include "command.h"


namespace RZE {

  class RenderContext;

	class Command_VertexBuffer : public Command {
	public:

		Command_VertexBuffer();
		~Command_VertexBuffer();

		void init(RenderContext *ctx, int32 total_byte_size, uint32 vertex_buffer_int);
		void setData(const float32 *positions, int32 p_size, const float32 *normals = nullptr, int32 n_size = 0, const float32 *uvs = nullptr, int32 u_size = 0);
    void execute() override;

	private:
		RenderContext *ctx_;
		float32 *vertices_;
		int32 array_size_;
		int32 positions_size_;
		int32 normals_size_;
		int32 uvs_size_;
		uint32 vertex_buffer_int_;

	};
}


#endif //COMMAND_VERTEX_BUFFER_H_