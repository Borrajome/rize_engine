#ifndef __PLATFORM_TYPES_H__
#define __PLATFORM_TYPES_H__

#include <stdint.h>

typedef uint8_t 		uchar8;
typedef int_least8_t 	char8;
typedef uint16_t 		ushort16;
typedef int_least16_t 	short16;
typedef uint32_t 		uint32;
typedef int_least32_t 	int32;
typedef uint64_t 		uint64;
typedef int_least64_t	int64;
typedef float           float32;
typedef double			float64;


#endif