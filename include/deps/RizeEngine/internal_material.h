#ifndef INTERNAL_MATERIAL_H
#define INTERNAL_MATERIAL_H 1

#include "mat4x4.hpp"
#include "platform_types.h"

namespace RZE {

  class InternalMaterial {
  public:

    InternalMaterial();
    ~InternalMaterial();

    void reset();

    void setGPUID(uint32 GPUID);
    void setInit(bool init);
    void setRender(bool render);

    uint32 GPUID();
    bool hasInitialized();
    bool hasRendered();

  private:
    uint32 GPUID_;
    bool render_;
    bool init_;

    // Material Variables
	
  };
}


#endif //INTERNAL_MATERIAL_H