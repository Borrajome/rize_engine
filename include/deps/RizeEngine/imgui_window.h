// ----------------------------------------------------------------------------
// Copyright (C) Rize Engine / Mario Borrajo Megoya / Jorge Virgos Castejon
// Copyright (C) 2018 Mario Borrajo Megoya 
// ImGui Window Management Wrapper Class.
// ----------------------------------------------------------------------------

#ifndef __IMGUI_WINDOW_H__
#define __IMGUI_WINDOW_H__ 1

//Headers
#include "deps/imgui/imgui.h"
#include "deps/imgui/imgui_impl_glfw_gl3.h"

struct Button {
	bool button_active_ = true;
	bool play_active_ = true;
	bool cross_active_ = true;
};

namespace RZE {

	/// @brief Class, this class is a wrapper of varius ImGui Methods.

	class ImguiWindow {
	public:

		/// @brief Default constructor.
		ImguiWindow();
		/// @brief Default copy constructor.
		ImguiWindow(ImguiWindow& other);
		/// @brief Default destructor.
		~ImguiWindow();

		//Methods

		/// @brief Inits a Imgui Windows with a position, scale and name
		/// @param w_pos corresponds the window position
		/// @param w_scale corresponds the window scale
		/// @param name_ correspond the window name
		void imguiStaticWindowInit(ImVec2 w_pos, ImVec2 w_scale, const char* name_);

		/// @brief Inits a moveable window 
		/// @param w_scale corresponds the window scale
		/// @param name_ correspond the window name
		void imguiMovableWindowInit(ImVec2 w_scale, const char* name_);

		/// @brief Close the ImGui window 
		void imguiWindowShutdown();

		/// @brief set the text with a default color
		/// @string corresponds the introduce text
		void imguiTextStyle(const char* string);

		/// @brief group varius style methods of ImGui
		void imguiButtonStyle();
		void imguiButtonPlayStyle();
		void imguiButtonStopStyle();


		Button buttons[5];

	private:
		ImVec4 windowColor = ImColor(107, 107, 107, 205);
		ImVec4 textColor = ImColor(0, 0, 0, 255);

		ImVec4 buttonPlayColor = ImColor(12, 116, 14, 255);
		ImVec4 buttonPlayHoverColor = ImColor(70, 154, 52, 200);
		ImVec4 buttonStopColor = ImColor(142, 13, 12, 255);
		ImVec4 buttonStopHoverColor = ImColor(184, 30, 28, 200);
		ImVec4 buttonNormalColor = ImColor(20, 20, 20, 255);
		ImVec4 buttonHoverColor = ImColor(60, 60, 60, 255);
		ImVec4 buttonPressColor = ImColor(80, 80, 80, 255);

		bool* p_open = NULL;

		bool no_titlebar = false;
		bool no_border = true;
		bool no_resize = true;
		bool no_move = false;
		bool no_scrollbar = true;
		bool no_collapse = true;
		bool no_menu = true;

		ImGuiWindowFlags staticWindowFlags = 0;
		ImGuiWindowFlags movableWindowFlags = 0;

	};
}
#endif
