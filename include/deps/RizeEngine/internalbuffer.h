#ifndef INTERNAL_BUFFER_H_
#define INTERNAL_BUFFER_H_ 1

#include "platform_types.h"

namespace RZE {
	class InternalBuffer {
	public:

		InternalBuffer();
		~InternalBuffer();

		void init(uint32 size);
		void reset();
		void setSizes(uint32 positions_size, uint32 normals_size, uint32 uvs_size);
		void setGPUID(uint32 GPUID);
    void setInit(bool init);
    void setRender(bool render);

		uint32 totalSize();
		uint32 posSize();
		uint32 norSize();
		uint32 uSize();
    uint32 getGPUID();
		bool getRender();
		bool getInit();


	private:
	
		uint32 GPUID_;
		bool render_;
		bool init_;
		unsigned int size_;

		uint32 total_size_;
		uint32 positions_size_;
		uint32 normals_size_;
		uint32 uvs_size_;


	};
}

#endif // INTERNAL_BUFFER_H_
