#ifndef GEO_PRIMITIVES_H_
#define GEO_PRIMITIVES_H_ 1

#include "glm.hpp"
#include <vector>
#include "render_context.h"
#include "display_list.h"
#include "obj_data_structs.h"

namespace RZE {

class GeoPrimitives {
public:
	static RenderObjectIndexes RenderCube(RenderContext *ctx, DisplayList *dl, RZE::kShaderType shader_type, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation_axis, float32 angle);
	static RenderObjectIndexes RenderPiramid(RenderContext *ctx, DisplayList *dl, RZE::kShaderType shader_type, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation_axis, float32 angle);
	static RenderObjectIndexes RenderSphere(RenderContext *ctx, DisplayList *dl, float radius, int sectorCount, int stackCount,RZE::kShaderType shader_type, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation_axis, float32 angle);
};


}

#endif // GEO_PRIMITIVES_H_