#ifndef AUDIO_SPECTRUM_VISUALIZER_H_
#define AUDIO_SPECTRUM_VISUALIZER_H_ 1

#include "platform_types.h"
#include "audio_loader.h"

namespace RZE {

  struct RGBA_color {
    char R;
    char G;
    char B;
    char A;
  };

  class AudioSpectrumVisualizer {
  public:
    AudioSpectrumVisualizer();
    ~AudioSpectrumVisualizer();
    
    /// \brief Creates a .png image of 1000x300 pixels from the name of an already loaded audio.
    /// The creation of the image depends on the loaded audios in the AudioLoader. The samples of the audio
    /// are converted into median values, which are then printed into the image into columns of 1px wide.
    /// The .png images are currently outputted to "./resources/images", and have the same name as the audio.
    static int32 GenerateSpectrumImage(char *audio_path, AudioLoader* audio_loader, RGBA_color back_color, RGBA_color graph_color);
  };
}

#endif //AUDIO_SPECTRUM_VISUALIZER_H_