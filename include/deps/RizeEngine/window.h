// ----------------------------------------------------------------------------
// Copyright (C) Rize Engine / Mario Borrajo Megoya / Jorge Virgos Castejon
// Copyright (C) 2018 Mario Borrajo Megoya 
// Window Management Class.
// ----------------------------------------------------------------------------

#ifndef WINDOW_CLASS_H_
#define WINDOW_CLASS_H_ 1

#define GLEW_STATIC

#include "deps/GLEW/glew.h"
#include "deps/GLFW/glfw3.h"

#include "deps/imgui/imgui.h"
#include "deps/imgui/imgui_impl_glfw_gl3.h"

#include "string"
#include "platform_types.h"
#include <stdlib.h>
#include <vector>

namespace RZE {

  // DONT CHANGE THE ORDER, ADD KEYS ONLY AT THE END
  // New keys have to be mapped in the constructor.
  enum InputKey {
    KINPUT_KEY_SPACE              = 0,
    KINPUT_KEY_APOSTROPHE         ,  /* ' */
    KINPUT_KEY_COMMA              ,  /* , */
    KINPUT_KEY_MINUS              ,  /* - */
    KINPUT_KEY_PERIOD             ,  /* . */
    KINPUT_KEY_SLASH              ,  /* / */
    KINPUT_KEY_0                  ,
    KINPUT_KEY_1                  ,
    KINPUT_KEY_2                  ,
    KINPUT_KEY_3                  ,
    KINPUT_KEY_4                  ,
    KINPUT_KEY_5                  ,
    KINPUT_KEY_6                  ,
    KINPUT_KEY_7                  ,
    KINPUT_KEY_8                  ,
    KINPUT_KEY_9                  ,
    KINPUT_KEY_SEMICOLON          ,  /* ; */
    KINPUT_KEY_EQUAL              ,  /* = */
    KINPUT_KEY_A                  ,
    KINPUT_KEY_B                  ,
    KINPUT_KEY_C                  ,
    KINPUT_KEY_D                  ,
    KINPUT_KEY_E                  ,
    KINPUT_KEY_F                  ,
    KINPUT_KEY_G                  ,
    KINPUT_KEY_H                  ,
    KINPUT_KEY_I                  ,
    KINPUT_KEY_J                  ,
    KINPUT_KEY_K                  ,
    KINPUT_KEY_L                  ,
    KINPUT_KEY_M                  ,
    KINPUT_KEY_N                  ,
    KINPUT_KEY_O                  ,
    KINPUT_KEY_P                  ,
    KINPUT_KEY_Q                  ,
    KINPUT_KEY_R                  ,
    KINPUT_KEY_S                  ,
    KINPUT_KEY_T                  ,
    KINPUT_KEY_U                  ,
    KINPUT_KEY_V                  ,
    KINPUT_KEY_W                  ,
    KINPUT_KEY_X                  ,
    KINPUT_KEY_Y                  ,
    KINPUT_KEY_Z                  ,
    KINPUT_KEY_LEFT_BRACKET       ,  /* [ */
    KINPUT_KEY_BACKSLASH          ,  /* \ */
    KINPUT_KEY_RIGHT_BRACKET      ,  /* ] */
    KINPUT_KEY_GRAVE_ACCENT       ,  /* ` */
    KINPUT_KEY_ESCAPE             ,
    KINPUT_KEY_ENTER              ,
    KINPUT_KEY_TAB                ,
    KINPUT_KEY_BACKSPACE          ,
    KINPUT_KEY_INSERT             ,
    KINPUT_KEY_DELETE             ,
    KINPUT_KEY_RIGHT              ,
    KINPUT_KEY_LEFT               ,
    KINPUT_KEY_DOWN               ,
    KINPUT_KEY_UP                 ,
    KINPUT_KEY_PAGE_UP            ,
    KINPUT_KEY_PAGE_DOWN          ,
    KINPUT_KEY_HOME               ,
    KINPUT_KEY_END                ,
    KINPUT_KEY_CAPS_LOCK          ,
    KINPUT_KEY_SCROLL_LOCK        ,
    KINPUT_KEY_NUM_LOCK           ,
    KINPUT_KEY_PRINT_SCREEN       ,
    KINPUT_KEY_PAUSE              ,
    KINPUT_KEY_F1                 ,
    KINPUT_KEY_F2                 ,
    KINPUT_KEY_F3                 ,
    KINPUT_KEY_F4                 ,
    KINPUT_KEY_F5                 ,
    KINPUT_KEY_F6                 ,
    KINPUT_KEY_F7                 ,
    KINPUT_KEY_F8                 ,
    KINPUT_KEY_F9                 ,
    KINPUT_KEY_F10                ,
    KINPUT_KEY_F11                ,
    KINPUT_KEY_F12                ,
    KINPUT_KEY_KP_DECIMAL         ,
    KINPUT_KEY_KP_DIVIDE          ,
    KINPUT_KEY_KP_MULTIPLY        ,
    KINPUT_KEY_KP_SUBTRACT        ,
    KINPUT_KEY_KP_ADD             ,
    KINPUT_KEY_KP_ENTER           ,
    KINPUT_KEY_KP_EQUAL           ,
    KINPUT_KEY_LEFT_SHIFT         ,
    KINPUT_KEY_LEFT_CONTROL       ,
    KINPUT_KEY_LEFT_ALT           ,
    KINPUT_KEY_LEFT_SUPER         ,
    KINPUT_KEY_RIGHT_SHIFT        ,
    KINPUT_KEY_RIGHT_CONTROL      ,
    KINPUT_KEY_RIGHT_ALT          ,
    KINPUT_KEY_RIGHT_SUPER        ,
    KINPUT_KEY_MENU               ,

    KINPUT_MAX
  };


class Window {
public:

	Window();
	~Window();
	Window(const Window& other);

	bool initWindow(int32 width, int32 height);

	int32 width();
	int32 height();

	void shutdownWindow();

	bool processEvents();
	void InputService();

	void swap();

	bool getKey(InputKey key);
	bool getKeyDown(InputKey key);
	bool getKeyUp(InputKey key);

	void captureMouse(bool capture);
	void mousePosition(float64 *x, float64 *y);
	void setMousePosition(float64 x, float64 y);


	GLFWwindow *window;

private:

	int32 internalGetKey(InputKey key);

	int32 width_;
	int32 height_;
	std::string window_name_;

	std::vector<int32> enum_to_backend_;
	bool current_frame_input[(int32)InputKey::KINPUT_MAX];
	bool previous_frame_input[(int32)InputKey::KINPUT_MAX];
	float64 mouse_x_, mouse_y_;

	bool set_mouse_pos_;
	float64 set_mouse_x_, set_mouse_y_;

	bool capturing_mouse_;
	bool capture_mouse_state_;

};
}
#endif
