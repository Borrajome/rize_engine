#ifndef COMMAND_CREATE_MATERIAL_H_
#define COMMAND_CREATE_MATERIAL_H_ 1

#include "command.h"
#include <vector>
#include "mat3x3.hpp"
#include "mat4x4.hpp"


namespace RZE {

  class RenderContext;

	class Command_CreateMaterial : public Command {

	public:
		Command_CreateMaterial();
		~Command_CreateMaterial();


		void init(RenderContext *ctx, uint32 material_id, uint32 vertex_id, uint32 fragment_id);
		void execute()override;

	private:

    uint32 material_id_;
    uint32 vertex_id_;
    uint32 fragment_id_;

    RenderContext *ctx_;

		
	};

}
#endif //COMMAND_CREATE_SHADER_H_
