#ifndef NODE_H_
#define NODE_H_ 1

#include "platform_types.h"
#include "obj_data_structs.h"
#include "vec3.hpp"
#include "vec4.hpp"
#include "mat4x4.hpp"
#include <vector>

namespace RZE {

  class RenderContext;

	class Node {
	public:

		static Node parent_node(RenderContext *ctx);

		~Node();

    Node create_child();

		void set_render_context(RenderContext *ctx);
	
		void set_position(glm::vec3 pos);
		void set_position(float32 x, float32 y, float32 z);
		glm::vec3 position();

    // ROTATION AXIS CANT BE 0 IN ALL DIMENSIONS
		void set_rotation(glm::vec3 axis, float32 angle);
		void set_rotation(float32 x, float32 y, float32 z, float32 angle);
		glm::vec4 rotation();

		void set_scale(glm::vec3 scale);
		void set_scale(float32 x, float32 y, float32 z);
		glm::vec3 scale();

    void set_geo_indexes(uint32 vertex_id, uint32 index_id, uint32 material_id);
    void set_geo_indexes(RZE::RenderObjectIndexes geo_index);
    RZE::RenderObjectIndexes geo_indexes();
    
		uint32 id_;
	
	private:

    Node();
    Node(RenderContext *ctx, Node *parent_node);
		RenderContext *ctx_;
	};

	struct InternalNodes {
		glm::vec3 positions[100];
		glm::vec4 rotation[100];
		glm::vec3 scale[100];

    std::vector<uint32> child_ids_[100];
    RZE::RenderObjectIndexes geo_indexes_[100];

		bool used_nodes[100];

		glm::mat4 models[100];
	};
}

#endif //NODE_H_