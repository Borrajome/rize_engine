#ifndef TEXTURE_CLASS_H_
#define TEXTURE_CLASS_H_ 1

#include "command.h"
#include "deps/GLEW/glew.h"
#include "render_context.h"
#include "common_errors.h"
#include "platform_types.h"

namespace RZE {


	class Command_createTexture : public Command {

	public:
		Command_createTexture();
		~Command_createTexture();

		enum Type {
			T_1D,
			T_2D,
			T_3D,
			T_CUBEMAP
		};

		enum Format {
			F_R,
			F_RG,
			F_RGB,
			F_RGBA,
			F_DEPTH,
			F_DEPTH32
		};

		enum Filter {
			FL_NEAREST,
			FL_LINEAR,
			FL_NEAREST_MIPMAP_NEAREST,
			FL_LINEAR_MIP_NEAREST,
			FL_NEAREST_MIPMAP_LINEAR,
			FL_LINEAR_MIPMAP_LINEAR
		};

		enum Wrap {
			W_REPEAT,
			W_MIRRORED_REPEAT,
			W_CLAMP_TO_EDGE,
			W_CLAMP_TO_BORDER
		};

		enum Parameter {
			P_MIN_FILTER,
			P_MAG_FILTER,
			P_WRAP_S,
			P_WRAP_T
		};

		void loadTextureInfo(const char *filename);

		GLenum RZE_OPENGL(Type type);
		GLenum RZE_OPENGL(Format format);
		GLenum RZE_OPENGL(Filter filter);
		GLenum RZE_OPENGL(Wrap wrap);
		GLenum RZE_OPENGL(Parameter parameter);

		void init(RenderContext *ctx, const char *filename, uint32 texture_id, Type type, Parameter parameter,
			      Wrap wrap, Format format, Filter filter);

		void execute()override;


	private:
		unsigned int text_id_;
	    int text_unit_;
		int width_;
		int height_;
		int depth_;
		int nr_color_channels_;
		unsigned char *img_data_;

		RenderContext *ctx_;

		GLenum type_;
		GLenum format_;
		GLenum filter_;
		GLenum wrap_;
		GLenum parameter_;

	};
}


#endif // !TEXTURE_CLASS_H_

