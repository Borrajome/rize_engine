#ifndef AUDIO_LOADER_H_
#define AUDIO_LOADER_H_ 1

#include <vector>
#include "platform_types.h"
#include "deps/FMOD/lowlevel/fmod.hpp"
#include "deps/FMOD/studio/fmod_studio.hpp"
class System;

namespace RZE  {
  
  class Track;

  /// \brief Contains the needed data to load a sound into FMOD or SoLoud
  struct RawAudio {
    char name[128]; ///< identifier of the structure. The main way to search for a specific sound
    float* data; ///< Pointer to the data. Must not be freed until all the FMOD sounds and SoLoud are released
    uint64 sample_count; ///< Number of samples pointed at in data
    uint32 sample_rate; ///< Sample rate of the sound
    ushort16 channels; ///< Number of audio channels
  };
  
  class AudioLoader {
  public:
    AudioLoader();
    ~AudioLoader();

    /// \brief Return the RawData object associated to the given name.
    /// Returns nullptr if the name is not found. This method does not load data, it just checks for the given
    /// name to be in the loaded sounds.
    RawAudio* audioInfo(char* audio_name);

  #ifdef FMOD_AUDIO
    /// \brief Loads a sound, or uses the one already loades if the name coincides.
    int32 loadAudio(FMOD::System *system, Track *track, char* audio_name);
  private:

    /// \brief Loads a Wav sound using dr_wav decoding library
    int32 loadWav(FMOD::System *system, Track *track, char* audio_name);
    /// \brief Loads a Flac sound using dr_wav decoding library
    int32 loadFlac(FMOD::System *system, Track *track, char* audio_name);
  #else
    /// \brief Loads a sound, or uses the one already loades if the name coincides.
    int32 loadAudio(Track *track, char* audio_name);
  private:

    /// \brief Loads a Wav sound using dr_wav decoding library
    int32 loadWav(Track *track, char* audio_name);
    /// \brief Loads a Flac sound using dr_wav decoding library
    int32 loadFlac(Track *track, char* audio_name);
  #endif

    std::vector<RawAudio> loaded_audio; ///< THe place the loaded audios are stored
  };
}


#endif // AUDIO_LOADER_H_