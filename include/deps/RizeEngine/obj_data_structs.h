#ifndef OBJ_DATA_STRUCTS_H_
#define OBJ_DATA_STRUCTS_H_ 1


#include <vector>
#include <string.h>
#include "platform_types.h"

namespace RZE {

	struct RenderObject
	{
		std::vector<float32> positions;
		std::vector<float32> normals;
		std::vector<float32> textcoords;
		std::vector<float32> colors;
		std::vector<ushort16> indices;
	};

	struct Index {
		int32 vertex_index;
		int32 normal_index;
		int32 texcoord_index;
	};

	struct Mesh {
		std::vector<Index> indices;

	};

	struct Shapes {

		Mesh mesh;


	};

  struct RenderObjectIndexes {
    uint32 vertex_id;
    uint32 index_id;
    uint32 material_id;
  };

  enum kShaderType {
    kShaderTypeRed = 0,
    kShaderTypeNormals,
    kShaderTypeUvs,
  };

}

#endif //OBJ_DATA_STRUCTS_H_