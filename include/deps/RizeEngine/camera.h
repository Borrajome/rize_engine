#ifndef CAMERA_H_
#define CAMERA_H_ 1

#include "vec3.hpp"
#include "mat4x4.hpp"
#include "window.h"
#include "platform_types.h"

namespace RZE {

  class Camera {
  public:
    Camera();
    ~Camera();

    void init(glm::vec3 camera_position, glm::vec3 camera_up, float32 yaw, float32 pitch);
    void changeSpeed(float32 speed, float32 rotation_speed, float32 mouse_s);
    void setCameraControlKey(InputKey key);

    void keyboardInput(Window *win);
    void mouseInput(Window *win, bool reset_mouse);
    void resetInput();
    void logic(float32 delta_time);

    void mouseMode(Window *win, bool mouse_control);

    glm::mat4 getViewMatrix();
    glm::mat4 getViewProjectionMatrix(float32 fov, float32 width, float32 height, float32 near, float32 far);


  private:

    void updateVectors();

    bool up_;
    bool down_;
    bool right_;
    bool left_;

    int32 key_x_rotation_;
    int32 key_y_rotation_;

    float64 mouse_x_rotation_;
    float64 mouse_y_rotation_;

    float32 speed_;
    float32 rot_speed_;
    float32 sensitivity_;
    float32 yaw_;
    float32 pitch_;

    glm::vec3 camera_position_;
    glm::vec3 camera_up_;
    glm::vec3 camera_front_;
    glm::vec3 camera_right_;

    float64 previous_mouse_x_position_;
    float64 previous_mouse_y_position_;

    InputKey camera_control_key_;
    bool mouse_controlling_camera_;
  };

}

#endif // CAMERA_H_