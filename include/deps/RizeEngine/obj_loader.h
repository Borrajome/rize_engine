#ifndef OBJ_LOADER_H_
#define OBJ_LOADER_H_ 1

#include "obj_data_structs.h"
#include "display_list.h"
#include "platform_types.h"

namespace RZE {

class ObjLoader {
public:
		ObjLoader() {};
		~ObjLoader() {};

		static std::vector<RenderObjectIndexes> loadObjRender(
      RZE::RenderContext* ctx, DisplayList* dl, RZE::kShaderType shader_type, glm::mat4 initial_model,
      const char8* obj_name, const char8* obj_mtl = nullptr);

		static std::vector<RZE::RenderObject>* loadObj(const char8* model_name,
			const char8* mtl_name = nullptr);
	};
}

#endif //OBJ_LOADER_H_