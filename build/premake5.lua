--Generate Solution

windows_sdk_version = "10.0.17134.0"

workspace "RizeEngine"
    configurations { "Debug", "Release" }
    location "../project"

files {
    "../include/*.h",

    --RaiZeEngine
    "../include/deps/RizeEngine/*.h",
    "../src/deps/RizeEngine/*.cpp",
    --
    
    -- GLFW
    "../include/deps/GLFW/*.h",
    "../src/deps/GLFW/egl_context.h",
    "../src/deps/GLFW/glfw_config.h",
    "../src/deps/GLFW/internal.h",
    "../src/deps/GLFW/wgl_context.h",
    "../src/deps/GLFW/win32_joystick.h",
    "../src/deps/GLFW/win32_platform.h",
    "../src/deps/GLFW/context.c",
    "../src/deps/GLFW/egl_context.c",
    "../src/deps/GLFW/init.c",
    "../src/deps/GLFW/input.c",
    "../src/deps/GLFW/monitor.c",
    "../src/deps/GLFW/vulkan.c",
    "../src/deps/GLFW/wgl_context.c",
    "../src/deps/GLFW/win32_init.c",
    "../src/deps/GLFW/win32_joystick.c",
    "../src/deps/GLFW/win32_monitor.c",
    "../src/deps/GLFW/win32_time.c",
    "../src/deps/GLFW/win32_time.c",
    "../src/deps/GLFW/win32_time.c",
    "../src/deps/GLFW/win32_tls.c",
    "../src/deps/GLFW/win32_window.c",
    "../src/deps/GLFW/window.c",
    --

    -- Sqlite
    --"../src/deps/sqlite/*.c",
    --"../include/deps/sqlite/*.h",
    --

    -- Lua
    "../src/deps/Lua/*.c",
    "../src/deps/Lua/*.cpp",
    "../include/deps/Lua/*.h",
    "../include/deps/Lua/*.hpp",
    --

    -- GLEW
    "../src/deps/GLEW/*.c",
    "../include/deps/GLEW/*.h",
    --

    -- Imgui
    "../include/deps/Imgui/*.h",
    "../include/deps/Imgui/*.hpp",
    "../src/deps/Imgui/*.cpp",
    "../src/deps/Imgui/*.cc",
    --

    -- GLM
    "../include/deps/glm/*.hpp",

    "../include/deps/glm/detail/*.hpp",
    "../include/deps/glm/detail/*.inl",

    "../include/deps/glm/ext/*.hpp",
    "../include/deps/glm/ext/*.inl",

    "../include/deps/glm/gtc/*.hpp",
    "../include/deps/glm/gtc/*.inl",

    "../include/deps/glm/gtx/*.hpp",
    "../include/deps/glm/gtx/*.inl",

    "../include/deps/glm/simd/*.hpp",
    "../include/deps/glm/simd/*.inl",

    "../src/deps/glm/detail/*.cpp",
    --

    -- PX
    "../include/deps/PX/*.h",
    --

    -- FMOD
    "../include/deps/FMOD/lowlevel/*.h",
    "../include/deps/FMOD/lowlevel/*.cs",
    "../include/deps/FMOD/lowlevel/*.hpp",
    "../include/deps/FMOD/studio/*.h",
    "../include/deps/FMOD/studio/*.hpp",
    "../include/deps/FMOD/studio/*.cs",
    --

    -- SoLoud
    "../include/deps/SoLoud/*.h",
    "../src/deps/SoLoud/**.c*",
    "../src/deps/SoLoud/**.h*",
    --

    -- SpdLog
    "../include/deps/spdlog/**.h",
    --

    --Others
    "../include/tiny_gltf.h",
    "../include/deps/stb_image.h",
    "../include/deps/stb_truetype.h",
    "../include/deps/tiny_obj_loader.h",
    "../include/deps/dr_wav.h",
    "../include/deps/dr_flac.h",
    "../include/deps/dr_mp3.h",
    --

    --Minitrace
    "../include/deps/minitrace/minitrace.h",
    "../src/deps/minitrace/minitrace.c"
    --
}

removefiles{
    "../include/deps/Lua/lua.h",
    "../src/deps/Lua/lua.c",
    "../src/deps/Lua/luac.c",

    "../src/deps/GLEW/glewinfo.c",
    "../src/deps/GLEW/visualinfo.c",

    "../src/deps/Soloud/backend/portaudio/soloud_portaudio_dll.c",
    "../src/deps/Soloud/backend/sdl/soloud_sdl1_dll.c",
    "../src/deps/Soloud/backend/sdl/soloud_sdl2_dll.c",
    "../src/deps/Soloud/backend/openal/soloud_openal_dll.c",

}

includedirs {"../include", "../deps", "../include/deps/sqlite", 
             "../include/deps/GLFW", "../include/deps/Lua",
             "../include/deps/GLEW","../include/deps/Imgui",
             "../include/deps/glm", "../include/deps/RizeEngine", 
             "../include/deps/PX", "../include/deps/FMOD/lowlevel",
             "../include/deps/FMOD/studio", "../include/deps/SoLoud",
             "../src/deps/SoLoud", "../include/deps/minitrace", "../include/deps"}

defines {"WIN32", "WINDOWS", "_GLFW_USE_CONFIG_H", "GLEW_STATIC", "WITH_WINMM", "WITH_WASAPI"}
systemversion(windows_sdk_version)

project "RMain"
    kind "ConsoleApp"
    language "C++"
    links {"opengl32.lib", "kernel32.lib",
     "user32.lib", "gdi32.lib", "winspool.lib",
     "shell32.lib", "ole32.lib", "oleaut32.lib",
     "uuid.lib", "comdlg32.lib", "advapi32.lib"}

     buildoptions { "/W4" }


    targetdir "../bin/%{cfg.buildcfg}"
    objdir "../build/obj/main"

    files {
        "../src/main.cpp"
    }

    removefiles{
        "../src/fractal.cpp",
        "../src/FractalExcersise.cpp"
    
    }

    filter "configurations:debug"
    defines {"DEBUG"}
    symbols "On"


    links{"../libs/Debug/fmod_vc.lib", "../libs/Debug/fmod64_vc.lib", 
          "../libs/Debug/fmodstudio_vc.lib", "../libs/Debug/fmodstudio64_vc.lib"}


    filter "configurations:Release"
        defines {"NDEBUG"}
        optimize "On"


    links{"../libs/Release/fmodL_vc.lib", "../libs/Release/fmodL64_vc.lib", 
          "../libs/Release/fmodstudioL_vc.lib", "../libs/Release/fmodstudioL64_vc.lib"}




    filter {"system:windows", "action:vs*"}


project "FractalTest"
    kind "ConsoleApp"
    language "C++"
    links {"opengl32.lib", "kernel32.lib",
     "user32.lib", "gdi32.lib", "winspool.lib",
     "shell32.lib", "ole32.lib", "oleaut32.lib",
     "uuid.lib", "comdlg32.lib", "advapi32.lib"}

     buildoptions { "/W4" }


    targetdir "../bin/%{cfg.buildcfg}"
    objdir "../build/obj/fractal"

    filter "configurations:debug"
    defines {"DEBUG"}
    symbols "On"


files {
    "../src/fractal.cpp"
}

filter "configurations:Release"
    defines {"NDEBUG"}
    optimize "On"

filter {"system:windows", "action:vs*"}


project "FMOD"
    kind "ConsoleApp"
    language "C++"
    links {"opengl32.lib", "kernel32.lib",
     "user32.lib", "gdi32.lib", "winspool.lib",
     "shell32.lib", "ole32.lib", "oleaut32.lib",
     "uuid.lib", "comdlg32.lib", "advapi32.lib"}

     buildoptions { "/W4" }


    targetdir "../bin/%{cfg.buildcfg}"
    objdir "../build/obj/main"

    files {
        "../src/FMODmain.cpp",
        "../src/common.cpp",
        "../src/common_platform.cpp",
        "../src/common_platform.h",
        "../src/common.h"
    }

    filter "configurations:debug"
    defines {"DEBUG"}
    symbols "On"

    links{"../libs/Debug/fmod_vc.lib", "../libs/Debug/fmod64_vc.lib", 
          "../libs/Debug/fmodstudio_vc.lib", "../libs/Debug/fmodstudio64_vc.lib"}

    filter "configurations:Release"
        defines {"NDEBUG"}
        optimize "On"

    links{"../libs/Release/fmodL_vc.lib", "../libs/Release/fmodL64_vc.lib", 
          "../libs/Release/fmodstudioL_vc.lib", "../libs/Release/fmodstudioL64_vc.lib"}

    filter {"system:windows", "action:vs*"}

    project "LuaTests"
    kind "ConsoleApp"
    language "C++"
    links {"opengl32.lib", "kernel32.lib",
     "user32.lib", "gdi32.lib", "winspool.lib",
     "shell32.lib", "ole32.lib", "oleaut32.lib",
     "uuid.lib", "comdlg32.lib", "advapi32.lib"}

     buildoptions { "/W4" }


    targetdir "../bin/%{cfg.buildcfg}"
    objdir "../build/obj/fractal"

    filter "configurations:debug"
    defines {"DEBUG"}
    symbols "On"


files {
    "../src/LuaMain.cpp"
}

removefiles{
    "../include/deps/RizeEngine/app.h",
    "../src/deps/RizeEngine/app.cpp",
    "../include/deps/RizeEngine/audio_system.h",
    "../src/deps/RizeEngine/audio_system.cpp"
}

filter "configurations:Release"
    defines {"NDEBUG"}
    optimize "On"

filter {"system:windows", "action:vs*"}


project "SoLoud"
    kind "ConsoleApp"
    language "C++"
    links {"opengl32.lib", "kernel32.lib",
     "user32.lib", "gdi32.lib", "winspool.lib",
     "shell32.lib", "ole32.lib", "oleaut32.lib",
     "uuid.lib", "comdlg32.lib", "advapi32.lib"}

     buildoptions { "/W4" }
    
    defines{
        "WITH_WINMM",
        "WITH_WASAPI",
    }

    targetdir "../bin/%{cfg.buildcfg}"
    objdir "../build/obj/main"

    files {
        "../include/deps/SoLoud/*.h",
        "../src/deps/SoLoud/backend/winm/**.c*",
        "../src/deps/SoLoud/backend/wasapi/**.c*",
        "../src/soloud_main.cpp",

    }

    filter "configurations:debug"
    defines {"DEBUG"}
    symbols "On"

    filter "configurations:Release"
        defines {"NDEBUG"}
        optimize "On"

    filter {"system:windows", "action:vs*"}


--End generate Solution

