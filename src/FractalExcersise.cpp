#define PX_SCHED_IMPLEMENTATION 1
#include "deps/PX/px_sched.h"
#include <thread>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include "deps/RizeEngine/clock.h"

using namespace std;

const int width = 512;
const int height = 512;

int maxIteration = 255;

double minRealValue = -1.5;
double maxRealValue = 1.0;

double minImaValue = -2.0;
double maxImaValue = 2.0;

int brightnes = 256;

int findIterations(double cr, double ci, int max_iterations) {

	int i = 0;
	double zr = 0.0;
	double zi = 0.0;

	while (i < max_iterations && zr * zr + zi * zi < 16.0) {

		double temp = zr * zr - zi * zi + cr;
		zi = 2.0 * zr * zi + ci;
		zr = temp;
		i++;
	}
	return i;
}


void mandelbrotSet() {

	//Open the output file, write the PPM header..
	ofstream fout("../examples/Fractals/mandelbrot_image.ppm");

	fout << "P3" << endl; // "Magic Number" - PPM file
	fout << width << " " << height << endl; // Dimensions
	fout << "256" << endl; // Max value of a pixel RGB value ...

	// For every pixel.. 

	for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					// Find the real and imaginary value for c, corresponding to that x, y pixel in the image
					double cr = x * ((maxRealValue - minRealValue) / width) + minRealValue;
					double ci = y * ((maxImaValue - minImaValue) / height) + minImaValue;
		
					// Find the number of iterations in the Mandelbrot formula using said c
					
					int n = findIterations(cr, ci, maxIteration);
					
					// Map the resulting number to an RGB value
					int r = (n % brightnes);
					int g = ((n * 3) % brightnes);
					int b = (n % brightnes);
					
					// Output it to the image
					fout << r << " " << g << " " << b << " ";
		
				}
				fout << endl;
	}
	fout.close();
	cout << "Finished!" << endl;
			

}

void JuliaSet() {

	// Open the output file, write the PPM header..
	ofstream fout("../examples/Fractals/julia_image.ppm");

	fout << "P3" << endl; // "Magic Number" - PPM file
	fout << width << " " << height << endl; // Dimensions
	fout << "256" << endl; // Max value of a pixel RGB value ...



	const double cr = -0.835;
	const double ci = -0.2321;

	double prevr, previ;

	for (unsigned int y = 0; y < height; ++y)
	{
		for (unsigned int x = 0; x < width; ++x)
		{
			double nextr = 1.5 * (2.0 * x / width - 1.0);
			double nexti = (2.0 * y / height - 1.0);

			for (unsigned int i = 0; i < maxIteration; ++i)
			{
				prevr = nextr;
				previ = nexti;

				nextr = prevr * prevr - previ * previ + cr;
				nexti = 2 * prevr * previ + ci;

				if (((nextr * nextr) + (nexti * nexti)) > 4)
				{
					int r = (i % brightnes);
					int g = ((i * 3) % brightnes);
					int b = (i % brightnes);

					// Output it to the image
					fout << r << " " << g << " " << b << " ";

					break;
				}
			}

		}
		fout << endl;
	}

	fout.close();
	cout << "Finished!" << endl;

}

void threadsIteration() {

	std::vector<std::string> vector_test;

	vector_test.reserve(10000);

	char buffer[20];

	for (int i = 0; i < 10000; i++)
	{
		vector_test.push_back(_itoa(i, buffer, 10));

	}

}

void threadIteration() {

	std::vector<std::string> vector_test;

	vector_test.reserve(80000);

	char buffer[20];

	for (int i = 0; i < 80000; i++)
	{
		vector_test.push_back(_itoa(i, buffer, 10));

	}

}

void MonoThread() {

	RZE::Clock clockMonothread;

	clockMonothread.start();


	threadIteration();

	clockMonothread.stop();

	printf("MonoThread: %f\n\n", clockMonothread.ellapsedMS());
}

void MultiThread() {
	RZE::Clock clockMultithread;

	clockMultithread.start();

	const unsigned int maxThreads = 8;

	std::thread threads[maxThreads];

	for (int i = 0; i < maxThreads; i++) {
		threads[i] = std::thread(threadsIteration);
	}

	for (int i = 0; i < maxThreads; i++) {
		threads[i].join();
	}

	clockMultithread.stop();

	printf("MultiThread: %f\n\n", clockMultithread.ellapsedMS());

}

void Scheduler() {

	RZE::Clock clockScheduler;

	px_sched::Scheduler schd;
	px_sched::SchedulerParams schdParams;
	schdParams.num_threads = 8;
	schdParams.max_running_threads = 8;
	schd.init(schdParams);

	px_sched::Sync s;

	clockScheduler.start();

	for (size_t i = 0; i < 8; ++i) {
		auto job = [] {
			threadsIteration();
		};
		// run a task, and notify to the given sync object
		schd.run(job, &s);
	}

	//printf("Waiting for tasks to finish...\n");
	schd.waitFor(s); // wait for all tasks to finish
	//printf("Waiting for tasks to finish...DONE \n");

	clockScheduler.stop();

	printf("Scheduler: %f\n\n", clockScheduler.ellapsedMS());

}


int main() {

	MonoThread();
	MultiThread();
	Scheduler();

	
	

	system("pause");
	return 0;
}