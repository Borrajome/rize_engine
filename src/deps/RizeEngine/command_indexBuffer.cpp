#include"command_indexBuffer.h"
#include "deps/GLEW/glew.h"
#include "render_context.h"
#include "spdlog\spdlog.h"


RZE::Command_IndexBuffer::Command_IndexBuffer() {

	indices_ = nullptr;
	index_buffer_int_ = 0;
}

RZE::Command_IndexBuffer::~Command_IndexBuffer() {

}


void RZE::Command_IndexBuffer::init(RenderContext *ctx, ushort16 *index, uint32 array_size, uint32 index_buffer_int) {

	
	indices_ = (ushort16*)malloc(sizeof(ushort16)*array_size);

	if (indices_ == nullptr) {
    spdlog::get("console")->debug("{}: Command_IndexBuffer MemoryAlloc error", __func__);
		return;
	}

	memcpy(indices_, index, array_size);

	array_size_ = array_size;
	index_buffer_int_ = index_buffer_int;

  ctx_ = ctx;
}

void RZE::Command_IndexBuffer::execute() {

	uint32 GPU_id;

	glGenBuffers(1, &GPU_id);

	ctx_->getInternalBuffer(index_buffer_int_)->setGPUID(GPU_id);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GPU_id);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, array_size_, indices_, GL_STATIC_DRAW);
}