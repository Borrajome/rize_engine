#include "command_createTexture.h"
#include "render_context.h"
#include "spdlog\spdlog.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"



RZE::Command_createTexture::Command_createTexture() {

	img_data_ = nullptr;
	text_id_ = 0;
}

RZE::Command_createTexture::~Command_createTexture() {

}


void RZE::Command_createTexture::loadTextureInfo(const char *filename) {

	stbi_set_flip_vertically_on_load(true);
	unsigned char *img_data_ = stbi_load("../resources/textures/Mario_block.png", &width_, &height_, &nr_color_channels_, 0);

	if (img_data_) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width_, height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, img_data_);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		spdlog::get("console")->error("Failed to load texture", __func__);
	}

}

GLenum RZE::Command_createTexture::RZE_OPENGL(Type type) {

	GLenum result;

	switch (type)
	{
	case Type::T_1D: result = GL_TEXTURE_1D;
		break;
	case Type::T_2D: result = GL_TEXTURE_2D;
		break;
	case Type::T_3D: result = GL_TEXTURE_3D;
		break;
	case Type::T_CUBEMAP: result = GL_TEXTURE_CUBE_MAP;
		break;
	}

	return result;
}

GLenum RZE::Command_createTexture::RZE_OPENGL(Format format) {

	GLenum result;

	switch (format)
	{
	case Format::F_R: result = GL_RED;
		break;
	case Format::F_RG: result = GL_RG;
		break;
	case Format::F_RGB: result = GL_RGB;
		break;
	case Format::F_RGBA: result = GL_RGBA;
		break;
	case Format::F_DEPTH: result = GL_DEPTH_COMPONENT;
		break;
	case Format::F_DEPTH32: result = GL_DEPTH_COMPONENT32;
		break;


	}

	return result;
}

GLenum RZE::Command_createTexture::RZE_OPENGL(Filter filter) {

	GLenum result;

	switch (filter)
	{
	case Filter::FL_NEAREST: result = GL_NEAREST;
		break;
	case Filter::FL_LINEAR: result = GL_LINEAR;
		break;
	case Filter::FL_NEAREST_MIPMAP_NEAREST: result = GL_NEAREST_MIPMAP_NEAREST;
		break;
	case Filter::FL_LINEAR_MIP_NEAREST: result = GL_LINEAR_MIPMAP_NEAREST;
		break;
	case Filter::FL_NEAREST_MIPMAP_LINEAR: result = GL_LINEAR_MIPMAP_LINEAR;
		break;
	case Filter::FL_LINEAR_MIPMAP_LINEAR: result = GL_LINEAR_MIPMAP_LINEAR;
		break;


	}

	return result;
}

GLenum RZE::Command_createTexture::RZE_OPENGL(Wrap wrap) {

	GLenum result;

	switch (wrap)
	{
	case Wrap::W_REPEAT: result = GL_REPEAT;
		break;
	case Wrap::W_MIRRORED_REPEAT: result = GL_MIRRORED_REPEAT;
		break;
	case Wrap::W_CLAMP_TO_EDGE: result = GL_CLAMP_TO_EDGE;
		break;
	case Wrap::W_CLAMP_TO_BORDER: result = GL_CLAMP_TO_BORDER;
		break;
	}

	return result;
}

GLenum RZE::Command_createTexture::RZE_OPENGL(Parameter parameter) {

	GLenum result;

	switch (parameter)
	{
	case Parameter::P_MIN_FILTER: result = GL_TEXTURE_MIN_FILTER;
		break;
	case Parameter::P_MAG_FILTER: result = GL_TEXTURE_MAG_FILTER;
		break;
	case Parameter::P_WRAP_S: result = GL_TEXTURE_WRAP_S;
		break;
	case Parameter::P_WRAP_T: result = GL_TEXTURE_WRAP_T;
		break;
	}

	return result;
}



void RZE::Command_createTexture::init(RenderContext *ctx, const char *filename, uint32 texture_id, Type type, Parameter parameter,
	                                  Wrap wrap, Format format, Filter filter) {

	ctx_ = ctx;
	text_id_ = texture_id;
	loadTextureInfo(filename);
	type_ = RZE_OPENGL(type);
	parameter_ = RZE_OPENGL(parameter);
	wrap_ = RZE_OPENGL(wrap);
	format_ = RZE_OPENGL(format);
	filter_ = RZE_OPENGL(filter);
}


void RZE::Command_createTexture::execute() {

	

	glGenTextures(1, &text_id_);

	ctx_->getInternalTexture(text_id_)->setGPUID(text_id_);
	ctx_->getInternalTexture(text_id_)->setTextureType(type_);

	text_unit_ = text_id_;

	glActiveTexture(GL_TEXTURE0 + text_unit_);
	glBindTexture(type_, text_id_);

	glTexParameteri(type_, GL_TEXTURE_WRAP_S, wrap_);
	glTexParameteri(type_, GL_TEXTURE_WRAP_T, wrap_);
	glTexParameteri(type_, GL_TEXTURE_MIN_FILTER, format_);
	glTexParameteri(type_, GL_TEXTURE_MIN_FILTER, format_);

	switch (type_)
	{
	case Type::T_1D: glTexImage1D(type_, 0, format_, width_,0, format_, GL_UNSIGNED_BYTE, img_data_);  break;
	case Type::T_2D: glTexImage2D(type_, 0, format_, width_, height_, 0, format_, GL_UNSIGNED_BYTE, img_data_); break;
	case Type::T_3D: glTexImage3D(type_, 0, format_, width_, height_, depth_, 0, format_, GL_UNSIGNED_BYTE, img_data_); break;

	}

	stbi_image_free(img_data_);

	glGenerateMipmap(type_);

}


