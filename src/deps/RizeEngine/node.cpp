#include "node.h"
#include "render_context.h"
#include "spdlog\spdlog.h"

RZE::Node::Node() {
  ctx_ = nullptr;
  id_ = 0;
}

RZE::Node::Node(RZE::RenderContext *ctx, Node *parent_node) {
	ctx_ = ctx;

	if (parent_node == nullptr) {
		id_ = 0;
    ctx_->internal_nodes().used_nodes[0] = true;
	}
	else {
		ctx_->initNode(this);
    ctx_->internal_nodes().child_ids_[parent_node->id_].push_back(id_);
	}
}

RZE::Node::~Node() {

}

RZE::Node RZE::Node::parent_node(RenderContext *ctx) {
	if(ctx == nullptr) return Node();
	else{
   return Node(ctx, nullptr);
  }
}

RZE::Node RZE::Node::create_child() {
    if(ctx_ != nullptr) return Node(ctx_, this);
    else {
      spdlog::get("console")->debug("{}: Creating node with Render Context set to nullptr", __func__);
      return Node();
    }
}

void RZE::Node::set_render_context(RZE::RenderContext *ctx) {
	ctx_ = ctx;
}

void RZE::Node::set_position(glm::vec3 pos) {
	if(nullptr != ctx_) ctx_->internal_nodes().positions[id_] = pos;
}

void RZE::Node::set_position(float32 x, float32 y, float32 z) {
	if (nullptr != ctx_) ctx_->internal_nodes().positions[id_] = glm::vec3(x, y, z);
}

glm::vec3 RZE::Node::position() {
	if (nullptr != ctx_) return ctx_->internal_nodes().positions[id_];
	else return glm::vec3(0.0f);
}


void RZE::Node::set_rotation(glm::vec3 axis, float32 angle) {
	if (nullptr != ctx_) ctx_->internal_nodes().rotation[id_] = glm::vec4(axis, angle);
}

void RZE::Node::set_rotation(float32 x, float32 y, float32 z, float32 angle) {
	if (nullptr != ctx_) ctx_->internal_nodes().rotation[id_] = glm::vec4(x, y, z, angle);
}

glm::vec4 RZE::Node::rotation() {
	if (nullptr != ctx_) return ctx_->internal_nodes().rotation[id_];
	else return glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
}

void RZE::Node::set_scale(glm::vec3 scale) {
	if (nullptr != ctx_) ctx_->internal_nodes().scale[id_] = scale;
}

void RZE::Node::set_scale(float32 x, float32 y, float32 z) {
	if (nullptr != ctx_) ctx_->internal_nodes().scale[id_] = glm::vec3(x, y, z);
}

glm::vec3 RZE::Node::scale() {
	if (nullptr != ctx_) return ctx_->internal_nodes().scale[id_];
	else return glm::vec3(1.0f);
}


void RZE::Node::set_geo_indexes(uint32 vertex_id, uint32 index_id, uint32 material_id) {
  if (nullptr != ctx_) ctx_->internal_nodes().geo_indexes_[id_] = RZE::RenderObjectIndexes{ vertex_id, index_id, material_id};
}

void RZE::Node::set_geo_indexes(RZE::RenderObjectIndexes geo_index) {
  if (nullptr != ctx_) ctx_->internal_nodes().geo_indexes_[id_] = geo_index;
}

RZE::RenderObjectIndexes RZE::Node::geo_indexes() {
  if (nullptr != ctx_) return ctx_->internal_nodes().geo_indexes_[id_];
  else return RZE::RenderObjectIndexes{ 0, 0, 0 };
}

