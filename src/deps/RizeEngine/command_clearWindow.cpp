#include "command_clearWindow.h"
#include "deps\GLEW\glew.h"

RZE::Command_ClearWindow::Command_ClearWindow() {}
RZE::Command_ClearWindow::~Command_ClearWindow() {}

void RZE::Command_ClearWindow::init(float32 color[4], int32 mask) {
  color_[0] = color[0];
  color_[1] = color[1];
  color_[2] = color[2];
  color_[3] = color[3];
  bit_mask_ = mask;
}

void RZE::Command_ClearWindow::execute() {
  glClearColor(color_[0], color_[1], color_[2], color_[3]);
  unsigned int gl_mask = 0;
  unsigned int local_mask = bit_mask_ & 1;
  if (local_mask == 1) gl_mask |= GL_COLOR_BUFFER_BIT;
  local_mask = bit_mask_ & 2;
  if (local_mask == 2) gl_mask |= GL_DEPTH_BUFFER_BIT;
  local_mask = bit_mask_ & 4;
  if (local_mask == 4) gl_mask |= GL_ACCUM_BUFFER_BIT;
  local_mask = bit_mask_ & 8;
  if (local_mask == 8) gl_mask |= GL_STENCIL_BUFFER_BIT;

  glClear(gl_mask);

  
}