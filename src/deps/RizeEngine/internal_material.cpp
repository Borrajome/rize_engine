#include "internal_material.h"
#include "deps/GLEW/glew.h"
#include "common_errors.h"


RZE::InternalMaterial::InternalMaterial() {
  GPUID_ = 0;
  render_ = false;
  init_ = false;
}

RZE::InternalMaterial::~InternalMaterial() {

}

void RZE::InternalMaterial::reset() {
  GPUID_ = 0;
  init_ = false;
  render_ = false;
}

void RZE::InternalMaterial::setGPUID(uint32 GPUID){
  GPUID_ = GPUID;
}

void RZE::InternalMaterial::setInit(bool init) {
  init_ = init;
}

void RZE::InternalMaterial::setRender(bool render) {
  render_ = render;
}


uint32 RZE::InternalMaterial::GPUID() {
  return GPUID_;
}

bool RZE::InternalMaterial::hasInitialized() {
  return init_;
}

bool RZE::InternalMaterial::hasRendered() {
  return render_;
}


