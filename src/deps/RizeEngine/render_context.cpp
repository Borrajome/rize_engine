#include "render_context.h"
#include "deps/GLEW/glew.h"
#include "common_errors.h"
#include <gtc/matrix_transform.hpp>

//#define GLSL(...) "#version 330 core\n" #VA_ARGS

RZE::RenderContext::RenderContext() {


	current_displayList = nullptr;
	current_displayList = (DisplayList*)malloc(sizeof(DisplayList));
	if (current_displayList == nullptr) {
		// Exception or crash
		spdlog::get("console")->error("{}: DisplayList MemoryAlloc", __func__);
	}


	buffers_size_ = 200;
	materials_size_ = 200;
	textures_size_ = 16;

	current_vertex_shader_id_ = 0;
	current_fragment_shader_id_ = 1;

	internal_buffer_ = (InternalBuffer*)malloc(sizeof(InternalBuffer) * buffers_size_);
	if (current_displayList == nullptr) {
		// Exception or crash
		spdlog::get("console")->error("{}: internal_buffer_ MemoryAlloc", __func__);
	}

	internal_materials_ = (InternalMaterial*)malloc(sizeof(InternalMaterial) * materials_size_);
	if (current_displayList == nullptr) {
		// Exception or crash
		spdlog::get("console")->error("{}: internal_materials_ MemoryAlloc", __func__);
	}

	internal_textures_ = (InternalTexture*)malloc(sizeof(InternalTexture) * textures_size_);
	if (current_displayList == nullptr) {
		// Exception or crash
		spdlog::get("console")->error("{}: internal_textures_ MemoryAlloc", __func__);
	}

	for (int32 i = 0; i < buffers_size_; ++i) internal_buffer_[i] = RZE::InternalBuffer();
	for (int32 i = 0; i < materials_size_; ++i) internal_materials_[i] = RZE::InternalMaterial();

	// Pre-loading the materials
	material_ids_[0] = createMaterial();
	material_ids_[1] = createMaterial();
	material_ids_[2] = createMaterial();

	char8 log[512] = "";
	vertex_shader_ids_[0] = glCreateShader(GL_VERTEX_SHADER);
	fragment_shader_ids_[0] = glCreateShader(GL_FRAGMENT_SHADER);
	fragment_shader_ids_[1] = glCreateShader(GL_FRAGMENT_SHADER);
	fragment_shader_ids_[2] = glCreateShader(GL_FRAGMENT_SHADER);

	const char vertex_source[] = {
	"#version 330 core \n"
	"uniform mat4 u_m_matrix;\n"
	"uniform mat4 u_vp_matrix;\n"
	"layout (location = 0) in vec3 a_position;\n"
	"layout (location = 1) in vec3 a_normal;\n"
	"layout (location = 2) in vec2 a_uv;\n"
	"out vec3 normal;\n"
	"out vec2 uv;\n"
	"mat4 Translate(vec3 move){\n"
	"mat4 result;\n"
	"result[0] = vec4(1,0,0,0);\n"
	"result[1] = vec4(0,1,0,0);\n"
	"result[2] = vec4(0,0,1,0);\n"
	"result[3] = vec4(move,1);\n"
    "return result;\n"
	"}\n"
	"void main(){\n"
	"mat4 model = Translate(vec3(0,0,0)) * u_m_matrix;\n"
	"  normal = (model * vec4(a_normal,0)).xyz;\n"
	"  normal = normalize(normal);\n"
	"  uv = a_uv;\n"
	"  gl_Position = (u_vp_matrix*u_m_matrix) * vec4(a_position, 1.0);\n"
	"}\n"
	};

	int vertex_shader_size = sizeof(vertex_source);
	const char* vertex_shader_ptr = vertex_source;
	

	glShaderSource(vertex_shader_ids_[0], 1, &vertex_shader_ptr, &vertex_shader_size);
	glCompileShader(vertex_shader_ids_[0]);
	glGetShaderInfoLog(vertex_shader_ids_[0], 512, 0, (char*)log);

	if ((char*)log != "") spdlog::get("console")->info("{}: vertexShaderLog - {}", __func__, log);


	const char fragment_source1[] = {
	"#version 330 core \n"
	"in vec3 normal;\n"
	"in vec2 uv;\n"
	"out vec4 FragColor;\n"
	"void main(){\n"
	"  vec3 light_direction = normalize(vec3(-1,-1,0));\n"
	"  float light_diffuse = max(dot(-light_direction , normal),0);\n"
	"  vec3 light = vec3(1.0,0.0,0.0) * (2.0*light_diffuse);\n"
	"  FragColor = vec4(light, 1.0);\n"
	"  //FragColor = vec4(normal, 1.0);\n"
	"  //FragColor = vec4(uv, 0.0, 1.0);\n"
	"}"
	};

	int fragment_shader1_size = sizeof(fragment_source1);
	const char* fragment_shader1_ptr = fragment_source1;

	glShaderSource(fragment_shader_ids_[0], 1, &fragment_shader1_ptr, &fragment_shader1_size);
	glCompileShader(fragment_shader_ids_[0]);
	glGetShaderInfoLog(fragment_shader_ids_[0], 512, 0, (char*)log);

	if ((char*)log != "") spdlog::get("console")->info("{}: fragmentShaderLog - {}", __func__, log);

	const char fragment_source2[] = {
	  "#version 330 core \n"
	  "in vec3 normal;\n"
	  "in vec2 uv;\n"
	  "out vec4 FragColor;\n"
	  "void main(){\n"
	  "  vec3 light_direction = normalize(vec3(-1,-1,0));\n"
	  "  float light_diffuse = max(dot(-light_direction , normal),0);\n"
	  "  //FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n"
	  "  FragColor = vec4(normal /* * (5.3*light_diffuse)*/, 1.0);\n"
	  "  //FragColor = vec4(uv, 0.0, 1.0);\n"
	  "}"
	};

	int fragment_shader2_size = sizeof(fragment_source2);
	const char* fragment_shader2_ptr = fragment_source2;

	glShaderSource(fragment_shader_ids_[1], 1, &fragment_shader2_ptr, &fragment_shader2_size);
	glCompileShader(fragment_shader_ids_[1]);
	glGetShaderInfoLog(fragment_shader_ids_[1], 512, 0, (char*)log);

	if ((char*)log != "") spdlog::get("console")->info("{}: fragmentShaderLog - {}", __func__, log);

	const char fragment_source3[] = {
	  "#version 330 core \n"
	  "in vec3 normal;\n"
	  "in vec2 uv;\n"
	  "out vec4 FragColor;\n"
	  "uniform sampler2D block_text;\n"
	  "void main(){\n"
	  "  //vec3 light_direction = normalize(vec3(-1,-1,0));\n"
	  "  //float light_diffuse = max(dot(-light_direction , normal),0);\n"
	  "  //FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n"
	  "  //FragColor = vec4(normal, 1.0);\n"
	  "  //FragColor = vec4(uv /** (5.3*light_diffuse)*/, 0.0, 1.0);\n"
	  "  FragColor = texture(block_text,uv);\n"
	  "}"
	};

	int fragment_shader3_size = sizeof(fragment_source3);
	const char* fragment_shader3_ptr = fragment_source3;

	glShaderSource(fragment_shader_ids_[2], 1, &fragment_shader3_ptr, &fragment_shader3_size);
	glCompileShader(fragment_shader_ids_[2]);
	glGetShaderInfoLog(fragment_shader_ids_[2], 512, 0, (char*)log);

	if ((char*)log != "") spdlog::get("console")->info("{}: fragmentShaderLog - {}", __func__, log);




	for (int i = 0; i < 100; ++i) {
		internal_nodes_.positions[i] = glm::vec3(0.0f);
		internal_nodes_.rotation[i] = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
		internal_nodes_.scale[i] = glm::vec3(1.0f);
		internal_nodes_.models[i] = glm::mat4(1.0f);

		internal_nodes_.geo_indexes_[i] = RZE::RenderObjectIndexes{ 0,0,0 };
		internal_nodes_.used_nodes[i] = false;

	}

}

RZE::RenderContext::~RenderContext() {

}



int32 RZE::RenderContext::copyDisplayList(DisplayList *display_list) {

	if (display_list == nullptr) {
		spdlog::get("console")->debug("{}: null display_list", __func__);
		return RZE::ErrorCode::kErrorCode_NullParam;
	}



	std::unique_lock<std::mutex> lock(m_push);
	cv_push.wait(lock, [this]() {
		return this->ready;
	});

	current_displayList = new DisplayList(*display_list);

	ready = false;

	cv_pop.notify_one();


	return RZE::ErrorCode::kErrorCode_Ok;
}

uint32 RZE::RenderContext::createBuffer(uint32 size) {

	for (uint32 i = 0; i < buffers_size_; i++)
	{
		if (internal_buffer_[i].getInit() == false) {

			internal_buffer_[i].reset();
			internal_buffer_[i].init(size);

			return i;
		}
	}

	spdlog::get("console")->debug("{}: full pool", __func__);
	return -1;
}

uint32 RZE::RenderContext::createMaterial() {
	for (uint32 i = 0; i < materials_size_; i++)
	{
		if (internal_materials_[i].hasInitialized() == false) {

			internal_materials_[i].reset();
			internal_materials_[i].setInit(true);

			return i;
		}
	}

	spdlog::get("console")->debug("{}: full pool", __func__);
	return -1;
}

RZE::InternalBuffer* RZE::RenderContext::getInternalBuffer(uint32 position) {

	if (position >= buffers_size_) {
		spdlog::get("console")->debug("{}: position overflow", __func__);
		return nullptr;
	}

	return &internal_buffer_[position];
}

RZE::InternalMaterial* RZE::RenderContext::getInternalMaterial(uint32 position) {

	if (position >= materials_size_) {
		spdlog::get("console")->debug("{}: position overflow", __func__);
		return nullptr;
	}

	return &internal_materials_[position];
}

RZE::InternalTexture* RZE::RenderContext::getInternalTexture(uint32 position) {
	if (position >= materials_size_) {
		spdlog::get("console")->debug("{}: position overflow", __func__);
		return nullptr;
	}

	return &internal_textures_[position];
}

void RZE::RenderContext::render() {

	if (current_displayList == nullptr) {
		spdlog::get("console")->debug("{}: null current_displayList", __func__);

		return;
	}

	std::unique_lock<std::mutex> lk(m_pop);
	cv_pop.wait(lk, [this]() {
		return this->current_displayList->getCommandList().size() > 0;
	});

	current_displayList->executeCommands();
	current_displayList->clearCommandList();

	ready = true;
	cv_push.notify_one();

}

uint32 RZE::RenderContext::get_vertex_shader_id(uint32 id) {
	return vertex_shader_ids_[id];
}
uint32 RZE::RenderContext::get_fragment_shader_id(uint32 id) {
	return fragment_shader_ids_[id];
}

int32 RZE::RenderContext::updateViewMatrix(glm::mat4 view_matrix) {
	view_matrix_ = view_matrix;
	return RZE::ErrorCode::kErrorCode_Ok;
}

glm::mat4 RZE::RenderContext::viewMatrix() {
	return view_matrix_;
}

RZE::InternalNodes& RZE::RenderContext::internal_nodes() {
	return internal_nodes_;
}

void RZE::RenderContext::initNode(RZE::Node *node) {
	for (uint32 i = 0; i < 100; i++)
	{
		if (internal_nodes_.used_nodes[i] == false) {

			internal_nodes_.used_nodes[i] = true;
			node->id_ = i;
			return;
		}
	}
}

void RZE::RenderContext::CalculateLocalModels() {
	for (uint32 i = 0; i < 100; i++)
	{
		if (internal_nodes_.used_nodes[i] == true) {

			glm::mat4 model = glm::translate(glm::mat4(1.0f), internal_nodes_.positions[i]);
			model = glm::rotate(model, internal_nodes_.rotation[i].w, glm::vec3(internal_nodes_.rotation[i].x, internal_nodes_.rotation[i].y, internal_nodes_.rotation[i].z));
			model = glm::scale(model, internal_nodes_.scale[i]);
			internal_nodes_.models[i] = model;
		}
	}
}

void RZE::RenderContext::CalculateNestedModels() {
	RecursiveModelCalculation(0);
}

void RZE::RenderContext::RenderNodes(DisplayList *dl) {
	for (uint32 i = 0; i < 100; i++)
	{
		if (internal_nodes_.used_nodes[i] == true) {
			if (internal_nodes_.geo_indexes_[i].vertex_id != 0 ||
				internal_nodes_.geo_indexes_[i].index_id != 0 ||
				internal_nodes_.geo_indexes_[i].material_id != 0) {

				dl->render(internal_nodes_.geo_indexes_[i].material_id,
					internal_nodes_.geo_indexes_[i].vertex_id,
					internal_nodes_.geo_indexes_[i].index_id, i);
			}
		}
	}
}


void RZE::RenderContext::RecursiveModelCalculation(uint32 node_id) {
	for (int i = 0; i < internal_nodes_.child_ids_[node_id].size(); ++i) {

		uint32 child_id = internal_nodes_.child_ids_[node_id][i];

		internal_nodes_.models[child_id] = internal_nodes_.models[node_id] * internal_nodes_.models[child_id];
		RecursiveModelCalculation(child_id);
	}
}

