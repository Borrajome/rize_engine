//#include "app.h"
//
//RZE::App::App() {
//	time_step_ = 16.0f;
//}
//
//RZE::App::~App() {
//
//}
//
//void RZE::App::init() {
//
//	//SpdLog
//	spdlog::init_thread_pool(8192, 1);
//	spdlog::flush_every(std::chrono::seconds(5));
//	spdlog::get("console") = spdlog::create_async_nb<spdlog::sinks::stdout_color_sink_mt>("console");
//	spdlog::get("console")->info("Asyncronous logger ready");
//	//
//
//	mainWindow.initWindow(1000, 1000);
//	render_ctx = new RZE::RenderContext();
//
//	//Schd
//	schd_params.num_threads = 8;
//	schd_params.max_running_threads = 8;
//	schd.init(schd_params);
//	//
//
//	clear_color[0] = 0.5f;
//	clear_color[1] = 0.5f;
//	clear_color[2] = 0.5f;
//	clear_color[3] = 1.0f;
//
//	screen_mask = RZE::ClearBit::kClearBit_Color | RZE::ClearBit::kClearBit_Depth;
//
//	srand(time(NULL));
//
//	auto logic_init_job = [this] { this->logic_init();};
//	schd.run(logic_init_job, &logic_init_sync_);
//	schd.waitFor(logic_init_sync_);
//}
//
//void RZE::App::start() {
//	auto logic_update_job = [this] { this->logic_update(); };
//
//	double current_time = (float)glfwGetTime();
//	double accum_time = 0;
//
//	while (mainWindow.processEvents()) {
//		inputService();
//		accum_time = (float)glfwGetTime() - current_time;
//
//		while (accum_time >= time_step_) {
//
//			render_ctx->updateViewMatrix(camera.getViewProjectionMatrix(70.0f, 1280.0f, 1280.0f, 0.1f, 100.0f));
//		
//
//			schd.run(logic_update_job, &logic_update_sync_);
//			current_time += time_step_;
//			accum_time = (float)glfwGetTime() - current_time;
//		}
//		draw();
//	}
//}
//
//void RZE::App::inputService() {
//	mainWindow.InputService();
//}
//
//void RZE::App::shutDown() {
//
//	spdlog::shutdown();
//	mainWindow.shutdownWindow();
//}
//
//void RZE::App::logic_init() {
//
//	logic_dl = new RZE::DisplayList();
//
//	std::vector<RZE::RenderObjectIndexes> obj;
//	obj = RZE::ObjLoader::loadObjRender(render_ctx, logic_dl, glm::identity<glm::mat4>(), (char8*)"../bin/obj/man.obj");
//
//	State_.vertex_id = obj[0].vertex_id;
//	State_.index_id = obj[0].index_id;
//	State_.material_id = obj[0].material_id;
//
//	camera.changeSpeed(2.0f, 50.0f, 0.10f);
//	movement = 0.0f;
//}
//
//void RZE::App::logic_update(/*float deltaTime*/) {
//
//	
//	logic_dl->clearWindow(clear_color, screen_mask);
//	
//
//	camera.mouseInput(&mainWindow, false);
//	camera.keyboardInput(&mainWindow);
//	camera.logic(0.1);
//
//	glm::mat4 model_mat = glm::identity<glm::mat4>();
//	model_mat = glm::scale(model_mat, glm::vec3(0.1f, 0.1f, 0.1f));
//	model_mat = glm::rotate(model_mat, movement, glm::vec3(0.0f, 1.0f, 0.0f));
//	render_ctx->updateModelMatrix(model_mat, State_.material_id);
//
//	movement += 0.03f;
//
//	logic_dl->render(State_.material_id, State_.vertex_id, State_.index_id);
//
//	render_ctx->copyDisplayList(logic_dl);
//
//	logic_dl->clearVector();
//
//}
//
//void RZE::App::draw() {
//	render_ctx->render();
//	mainWindow.swap();
//}
//
