#define TINYOBJLOADER_IMPLEMENTATION
#include "deps/tiny_obj_loader.h"
#include <vector>
#include <stdlib.h>
#include "obj_loader.h"
#include "render_context.h"
#include "common_errors.h"

std::vector<RZE::RenderObjectIndexes> RZE::ObjLoader::loadObjRender(RZE::RenderContext* ctx, RZE::DisplayList* dl, RZE::kShaderType shader_type, glm::mat4 initial_model, const char8* obj_name, const char8* obj_mtl) {

	std::vector<RZE::RenderObject> *obj;
	std::vector<RZE::RenderObjectIndexes> obj_indices;
	obj = RZE::ObjLoader::loadObj(obj_name, obj_mtl);

	obj_indices.reserve(obj->size());

	for (int32 i = 0; i < obj->size(); ++i) {
		int32 p_size = obj->data()->positions.size() * sizeof(float32);
		int32 n_size = obj->data()->normals.size() * sizeof(float32);
		int32 u_size = obj->data()->textcoords.size() * sizeof(float32);
		int32 index_size = obj->data()->indices.size() * sizeof(ushort16);

		float32 *positions = obj->data()->positions.data();
		float32 *normals = obj->data()->normals.data();
		float32 *uvs = obj->data()->textcoords.data();
		ushort16 *indices = obj->data()->indices.data();

		RenderObjectIndexes ids;
		ids.vertex_id = ctx->createBuffer(p_size + n_size + u_size);
		ids.index_id = ctx->createBuffer(index_size);

    // Should be mapped to the corresponding internal buffer
		ids.material_id = static_cast<uint32>(shader_type);

		dl->vertexBuffer(ids.vertex_id, positions, p_size, normals, n_size, uvs, u_size);
		dl->indexBuffer(indices, index_size, ids.index_id);

		obj_indices.push_back(ids);
	}

	return obj_indices;
}

std::vector<RZE::RenderObject>*RZE::ObjLoader::loadObj(const char8 *model_name, const char8 *mtl_name) {

	tinyobj::attrib_t attributes;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string error_code;
	tinyobj::LoadObj(&attributes, &shapes, &materials, &error_code, (char*)model_name, (char*)mtl_name);
	spdlog::get("console")->debug("{}: obj error - {}", __func__, error_code);

	if (shapes.size() == 0) return nullptr;

	std::vector<RZE::RenderObject> *geo_vector = new std::vector<RZE::RenderObject>();
	geo_vector->reserve(shapes.size());

	uint32 vertex_index = 0, normal_index = 0, texcoord_index = 0;
	uint64 index_key = 0;
	std::map<uint64, ushort16> index_lib;
	std::map<uint64, ushort16>::iterator it;
	ushort16 index_counter = 0;


	for (int32 i = 0; i < shapes.size(); ++i) {

		RZE::RenderObject geo;
		int32 indices_size = shapes[i].mesh.indices.size();

		bool use_normals = shapes[i].mesh.indices[0].normal_index != -1;
		bool use_uvs = shapes[i].mesh.indices[0].texcoord_index != -1;

		if (!use_normals) spdlog::get("console")->debug("{}: obj without normals", __func__);
		if (!use_uvs) spdlog::get("console")->debug("{}: obj without uvs", __func__);

		for (int32 j = 0; j < indices_size; ++j) {

			vertex_index = shapes[i].mesh.indices[j].vertex_index;
			normal_index = shapes[i].mesh.indices[j].normal_index;
			texcoord_index = shapes[i].mesh.indices[j].texcoord_index;

			index_key = vertex_index | normal_index << 24 | texcoord_index << 48;
			it = index_lib.find(index_key);

			if (it == index_lib.end()) {
				index_lib.insert(std::pair<uint64, ushort16>(index_key, index_counter));

				geo.positions.push_back(attributes.vertices[(vertex_index * 3) + 0]);
				geo.positions.push_back(attributes.vertices[(vertex_index * 3) + 1]);
				geo.positions.push_back(attributes.vertices[(vertex_index * 3) + 2]);

				if (use_normals) {
					geo.normals.push_back(attributes.normals[normal_index * 3 + 0]);
					geo.normals.push_back(attributes.normals[normal_index * 3 + 1]);
					geo.normals.push_back(attributes.normals[normal_index * 3 + 2]);
				}

				if (use_uvs) {
					geo.textcoords.push_back(attributes.texcoords[texcoord_index * 2 + 0]);
					geo.textcoords.push_back(attributes.texcoords[texcoord_index * 2 + 1]);
				}
				geo.indices.push_back(index_counter);

				index_counter++;

			}
			else {
				geo.indices.push_back(it->second);
			}
		}

		geo_vector->push_back(geo);
	}

	return geo_vector;
}