#include "window.h"

using namespace RZE;
using namespace ImGui;

RZE::Window::Window() {
	width_ = 1080;
	height_ = 720;
	window_name_ = "RiZeEngine";

  mouse_x_ = 0.0f;
  mouse_y_ = 0.0f;

  set_mouse_pos_ = false;
  capturing_mouse_ = false;
  capture_mouse_state_ = false;

  set_mouse_x_ = 0.0f;
  set_mouse_y_ = 0.0f;

  for (int32 i = 0; i < (int32)InputKey::KINPUT_MAX; ++i) {
    previous_frame_input[i] = false;
    current_frame_input[i] = false;
  }


  // Depending on the platform, the values will vary.
  // For now, values for GLFW input.

  enum_to_backend_.push_back(32);
  enum_to_backend_.push_back(39);

  // 0-9 plus comma, minus, period and slash
  for(int32 i = 44; i < 58; ++i){ enum_to_backend_.push_back(i); }

  enum_to_backend_.push_back(59);
  enum_to_backend_.push_back(61);

  // A-Z plus brackets and backslah
  for (int32 i = 65; i < 94; ++i) { enum_to_backend_.push_back(i); }

  enum_to_backend_.push_back(96);

  for (int32 i = 256; i < 270; ++i) { enum_to_backend_.push_back(i); }

  enum_to_backend_.push_back(280);
  enum_to_backend_.push_back(281);
  enum_to_backend_.push_back(282);
  enum_to_backend_.push_back(283);
  enum_to_backend_.push_back(284);

  for (int32 i = 290; i < 302; ++i) { enum_to_backend_.push_back(i); }

  for (int32 i = 330; i < 337; ++i) { enum_to_backend_.push_back(i); }

  for (int32 i = 340; i < 349; ++i) { enum_to_backend_.push_back(i); }



}

RZE::Window::~Window() {}

RZE::Window::Window(const Window &other) {
	width_ = other.width_;
	height_ = other.height_;
	window_name_ = other.window_name_;
}

bool Window::initWindow(int32 width, int32 height) {

	if (!glfwInit()) {
		printf("\nGLFW INIT ERROR\n");
		return false;
	}

	width_ = width;
	height_ = height;

	window = glfwCreateWindow(width_, height_, window_name_.c_str(), NULL, NULL);

	if (!window) {
		printf("\nGLFW WINDOW CREATION ERROR\n");

		glfwTerminate();
		return false;
	}

	glfwMakeContextCurrent(window);

	glfwMakeContextCurrent(window); // Initialize GLEW
	glewExperimental = true; // Needed in core profile

	if (glewInit() != GLEW_OK) {
		printf("\n GLEW INITIALIZE ERROR\n");
		return false;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	ImGui_ImplGlfwGL3_Init(window, true);

	glfwSwapInterval(1);

	return true;

}

int32 Window::width() { return width_; }
int32 Window::height() { return height_; }


void Window::shutdownWindow() {
	glfwDestroyWindow(window);
	glfwTerminate();
}

bool Window::processEvents() {
	return (getKey(InputKey::KINPUT_KEY_ESCAPE) != true && glfwWindowShouldClose(window) == 0);
}

void Window::swap() {
	glfwSwapBuffers(window);
}

bool Window::getKey(InputKey key) {
  return current_frame_input[(int32)key];
}

bool Window::getKeyDown(InputKey key) {
  return current_frame_input[(int32)key] && !previous_frame_input[(int32)key];
}

bool Window::getKeyUp(InputKey key) {
  return !current_frame_input[(int32)key] && previous_frame_input[(int32)key];
}


void Window::captureMouse(bool capture) {
  capturing_mouse_ = true;
  capture_mouse_state_ = capture;
}

void Window::mousePosition(float64 *x, float64 *y) {
  *x = mouse_x_;
  *y = mouse_y_;
}

void RZE::Window::setMousePosition(float64 x, float64 y) {
  set_mouse_pos_ = true;
  set_mouse_x_ = x;
  set_mouse_y_ = y;
}

int32 Window::internalGetKey(InputKey key) {
  return glfwGetKey(window, enum_to_backend_[key]);
}

void RZE::Window::InputService() {
  glfwPollEvents();

  for(int32 i = 0; i < (int32)InputKey::KINPUT_MAX; ++i){
    previous_frame_input[i] = current_frame_input[i];
    current_frame_input[i] = internalGetKey((RZE::InputKey)i);
  }

  if(set_mouse_pos_){
    glfwSetCursorPos(window, set_mouse_x_, set_mouse_y_);
    set_mouse_pos_ = false;
  }
  glfwGetCursorPos(window, &mouse_x_, &mouse_y_);

  if (capturing_mouse_){
    if(capture_mouse_state_) glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    else glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    capturing_mouse_ = false;
  }
}