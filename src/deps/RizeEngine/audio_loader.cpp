#include "audio_loader.h"
#include "common_errors.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "audio_system.h"


#define DR_WAV_IMPLEMENTATION
#include "..\SoLoud\audiosource\wav\dr_wav.h"
#define DR_FLAC_IMPLEMENTATION
#include "..\SoLoud\audiosource\wav\dr_flac.h"
#include "SoLoud\soloud_wav.h"

RZE::AudioLoader::AudioLoader() {

}

RZE::AudioLoader::~AudioLoader() {
  loaded_audio.clear();
}

RZE::RawAudio* RZE::AudioLoader::audioInfo(char* audio_name) {
  for (int i = 0; i < loaded_audio.size(); ++i) {
    if (strcmp(loaded_audio[i].name, audio_name) == 0) {
      return &loaded_audio[i];
    }
  }
  return nullptr;
}

#ifdef FMOD_AUDIO
int32 RZE::AudioLoader::loadAudio(FMOD::System *system, Track *track, char* audio_name) {

  for (int i = 0; i < loaded_audio.size(); ++i) {
    if (strcmp(loaded_audio[i].name, audio_name) == 0) {
      FMOD::Sound *tmpSound = nullptr;
      FMOD_CREATESOUNDEXINFO info = { 0 };
      info.cbsize = sizeof(info);
      info.defaultfrequency = loaded_audio[i].sample_rate;
      info.numchannels = loaded_audio[i].channels;
      info.format = FMOD_SOUND_FORMAT::FMOD_SOUND_FORMAT_PCMFLOAT;
      info.length = loaded_audio[i].sample_count * sizeof(float32);

      FMOD_RESULT error = system->createSound((char *)loaded_audio[i].data,
        FMOD_OPENRAW | FMOD_OPENMEMORY_POINT | FMOD_CREATESTREAM,
        &info, &tmpSound);

      track->sound_ = tmpSound;

      return ErrorCode::kErrorCode_Ok;
    }
  }

  int end = 0;
  while (audio_name[end] != '\0') end++;

  char type[4];
  type[0] = audio_name[end - 3];
  type[1] = audio_name[end - 2];
  type[2] = audio_name[end - 1];
  type[3] = '\0';

  if (strcmp(type, "wav") == 0) loadWav(system, track, audio_name);
  else if (strcmp(type, "lac") == 0) loadFlac(system, track, audio_name);
}

int32 RZE::AudioLoader::loadWav(FMOD::System *system, Track *track, char* audio_name) {


  drwav* pWav = drwav_open_file(audio_name);
  if (pWav == nullptr) {
    spdlog::get("console")->debug("{}: Error opening audio file", __func__);
    return ErrorCode::kErrorCode_NullParam;
  }

  float32* pSampleData = (float32*)malloc(pWav->totalSampleCount * sizeof(float32));
  drwav_read_pcm_frames_f32(pWav, pWav->totalSampleCount, pSampleData);

  FMOD::Sound *tmpSound = nullptr;
  FMOD_CREATESOUNDEXINFO info = { 0 };
  info.cbsize = sizeof(info);
  info.defaultfrequency = pWav->sampleRate;
  info.numchannels = pWav->channels;
  info.format = FMOD_SOUND_FORMAT::FMOD_SOUND_FORMAT_PCMFLOAT;
  info.length = pWav->totalSampleCount * sizeof(float32);

  FMOD_RESULT error = system->createSound((char *)pSampleData,
    FMOD_OPENRAW | FMOD_OPENMEMORY_POINT | FMOD_CREATESTREAM, 
    &info, &tmpSound);
  
  track->sound_ = tmpSound;

  RZE::RawAudio audio;
  strcpy_s(audio.name, audio_name);
  audio.data = pSampleData;
  audio.sample_count = pWav->totalSampleCount;
  audio.sample_rate = pWav->sampleRate;
  audio.channels = pWav->channels;

  loaded_audio.push_back(audio);
  return ErrorCode::kErrorCode_Ok;



}

int32 RZE::AudioLoader::loadFlac(FMOD::System *system, Track *track, char* audio_name) {
  drflac* pFlac = drflac_open_file(audio_name);
  if (pFlac == nullptr) {
    spdlog::get("console")->debug("{}: Error opening audio file", __func__);
    return ErrorCode::kErrorCode_NullParam;
  }

  float32* pSampleData = (float32*)malloc(pFlac->totalSampleCount * sizeof(float32));
  drflac_read_pcm_frames_f32(pFlac, pFlac->totalSampleCount, pSampleData);

  uint64 samples_per_channel = pFlac->totalSampleCount / pFlac->channels;

  FMOD::Sound *tmpSound = nullptr;
  FMOD_CREATESOUNDEXINFO info = { 0 };
  info.cbsize = sizeof(info);
  info.defaultfrequency = pFlac->sampleRate;
  info.numchannels = pFlac->channels;
  info.format = FMOD_SOUND_FORMAT::FMOD_SOUND_FORMAT_PCMFLOAT;
  info.length = pFlac->totalSampleCount * sizeof(float32);

  FMOD_RESULT error = system->createSound((char *)pSampleData,
    FMOD_OPENRAW | FMOD_OPENMEMORY_POINT | FMOD_CREATESTREAM,
    &info, &tmpSound);

  track->sound_ = tmpSound;

  RZE::RawAudio audio;
  strcpy_s(audio.name, audio_name);
  audio.data = pSampleData;
  audio.sample_count = pFlac->totalSampleCount;
  audio.sample_rate = pFlac->sampleRate;
  audio.channels = pFlac->channels;

  loaded_audio.push_back(audio);
  return ErrorCode::kErrorCode_Ok;

}
#else
int32 RZE::AudioLoader::loadAudio(Track *track, char* audio_name) {

  for (int i = 0; i < loaded_audio.size(); ++i) {
    if (strcmp(loaded_audio[i].name, audio_name) == 0) {
      SoLoud::Wav *raw_wav = new SoLoud::Wav();
      raw_wav->loadRawWave(loaded_audio[i].data, loaded_audio[i].sample_count,
        loaded_audio[i].sample_rate, loaded_audio[i].channels, true, false);
      track->sound_ = raw_wav;

      return ErrorCode::kErrorCode_Ok;
    }
  }

  int end = 0;
  while(audio_name[end] != '\0') end++;

  char type[4];
  type[0] = audio_name[end - 3];
  type[1] = audio_name[end - 2];
  type[2] = audio_name[end - 1];
  type[3] = '\0';

  if (strcmp(type, "wav") == 0) loadWav(track, audio_name);
  else if(strcmp(type, "lac") == 0) loadFlac(track, audio_name);
}

int32 RZE::AudioLoader::loadWav(Track *track, char* audio_name) {


  drwav* pWav = drwav_open_file(audio_name);
  if (pWav == nullptr) {
    spdlog::get("console")->debug("{}: Error opening audio file",__func__);
    return ErrorCode::kErrorCode_NullParam;
  }

  float32* pSampleData = (float32*)malloc(pWav->totalSampleCount * sizeof(float32));
  float32* reorderedData = (float32*)malloc(pWav->totalSampleCount * sizeof(float32));
  drwav_read_pcm_frames_f32(pWav, pWav->totalSampleCount, pSampleData);

  uint64 samples_per_channel = pWav->totalSampleCount / pWav->channels;

  for (uint32 i = 0; i < samples_per_channel; ++i) {
    for (uint32 j = 0; j < pWav->channels; ++j) {
      reorderedData[samples_per_channel * j + i] = pSampleData[i * pWav->channels + j];
    }
  }

  free(pSampleData);

  SoLoud::Wav *raw_wav = new SoLoud::Wav();
  raw_wav->loadRawWave(reorderedData, pWav->totalSampleCount, 
    pWav->sampleRate, pWav->channels, false, false);
  track->sound_ = raw_wav;

  RZE::RawAudio audio;
  strcpy_s(audio.name, audio_name);
  audio.data = reorderedData;
  audio.sample_count = pWav->totalSampleCount;
  audio.sample_rate = pWav->sampleRate;
  audio.channels = pWav->channels;
  
  loaded_audio.push_back(audio);
  return ErrorCode::kErrorCode_Ok;



}

int32 RZE::AudioLoader::loadFlac(Track *track, char* audio_name) {

  drflac* pFlac = drflac_open_file(audio_name);
  if (pFlac == nullptr) {
    spdlog::get("console")->debug("{}: Error opening audio file", __func__);
    return ErrorCode::kErrorCode_NullParam;
  }

  float32* pSampleData = (float32*)malloc(pFlac->totalSampleCount * sizeof(float32));
  float32* reorderedData = (float32*)malloc(pFlac->totalSampleCount * sizeof(float32));
  drflac_read_pcm_frames_f32(pFlac, pFlac->totalSampleCount, pSampleData);

  uint64 samples_per_channel = pFlac->totalSampleCount / pFlac->channels;

  for (uint32 i = 0; i < samples_per_channel; ++i) {
    for (uint32 j = 0; j < pFlac->channels; ++j) {
      reorderedData[samples_per_channel * j + i] = pSampleData[i * pFlac->channels + j];
    }
  }

  free(pSampleData);

  SoLoud::Wav *raw_wav = new SoLoud::Wav();
  raw_wav->loadRawWave(reorderedData, pFlac->totalSampleCount, 
    pFlac->sampleRate, pFlac->channels, false, false);
  track->sound_ = raw_wav;

  RZE::RawAudio audio;
  strcpy_s(audio.name, audio_name);
  audio.data = reorderedData;
  audio.sample_count = pFlac->totalSampleCount;
  audio.sample_rate = pFlac->sampleRate;
  audio.channels = pFlac->channels;

  loaded_audio.push_back(audio);
  return ErrorCode::kErrorCode_Ok;

}

#endif