#include "imgui_window.h"

using namespace RZE;

ImguiWindow::ImguiWindow() {
}

ImguiWindow::ImguiWindow(ImguiWindow& other) {}

ImguiWindow::~ImguiWindow() {};

void ImguiWindow::imguiStaticWindowInit(ImVec2 w_pos, ImVec2 w_scale, const char* name_) {

	ImGui::SetNextWindowSize(ImVec2(w_scale.x, w_scale.y));
	ImGui::SetNextWindowPos(ImVec2(w_pos.x, w_pos.y));
	ImGui::PushStyleColor(ImGuiCol_WindowBg, windowColor);


	if (no_titlebar)  staticWindowFlags |= ImGuiWindowFlags_NoTitleBar;
	if (!no_border)   staticWindowFlags |= ImGuiWindowFlags_ShowBorders;
	if (no_resize)    staticWindowFlags |= ImGuiWindowFlags_NoResize;
	if (no_move)      staticWindowFlags |= ImGuiWindowFlags_NoMove;
	if (no_scrollbar) staticWindowFlags |= ImGuiWindowFlags_NoScrollbar;
	if (no_collapse)  staticWindowFlags |= ImGuiWindowFlags_NoCollapse;
	if (!no_menu)     staticWindowFlags |= ImGuiWindowFlags_MenuBar;

	ImGui::Begin(name_, p_open, staticWindowFlags);


}

void ImguiWindow::imguiMovableWindowInit(ImVec2 w_scale, const char* name_) {

	ImGui::SetNextWindowSize(ImVec2(w_scale.x, w_scale.y));
	ImGui::PushStyleColor(ImGuiCol_WindowBg, windowColor);


	if (no_titlebar)  movableWindowFlags |= ImGuiWindowFlags_NoTitleBar;
	if (!no_border)   movableWindowFlags |= ImGuiWindowFlags_ShowBorders;
	if (no_resize)    movableWindowFlags |= ImGuiWindowFlags_NoResize;
	if (no_move)      movableWindowFlags |= ImGuiWindowFlags_NoMove;
	if (no_scrollbar) movableWindowFlags |= ImGuiWindowFlags_NoScrollbar;
	if (!no_collapse)  movableWindowFlags |= ImGuiWindowFlags_NoCollapse;
	if (!no_menu)     movableWindowFlags |= ImGuiWindowFlags_MenuBar;



	ImGui::Begin(name_, p_open, movableWindowFlags);

}

void ImguiWindow::imguiWindowShutdown() {

	ImGui::End();
	ImGui::PopStyleColor();

}

void ImguiWindow::imguiTextStyle(const char* string) {

	ImGui::TextColored(textColor, string);


}

void ImguiWindow::imguiButtonStyle() {

	ImGui::PushStyleColor(ImGuiCol_Button, buttonNormalColor);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, buttonHoverColor);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, buttonPressColor);

}

void ImguiWindow::imguiButtonPlayStyle() {

	ImGui::PushStyleColor(ImGuiCol_Button, buttonPlayColor);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, buttonPlayHoverColor);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, buttonPressColor);

}

void ImguiWindow::imguiButtonStopStyle() {

	ImGui::PushStyleColor(ImGuiCol_Button, buttonStopColor);
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, buttonStopHoverColor);
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, buttonPressColor);

}