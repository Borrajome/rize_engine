#include "display_list.h"
#include "render_context.h"
#include "common_errors.h"


RZE::DisplayList::DisplayList() {
  current_ctx_ = nullptr;
}

RZE::DisplayList::DisplayList(const DisplayList &displaylist_obj) {

	current_ctx_ = displaylist_obj.current_ctx_;

	command_list = displaylist_obj.command_list;
}

RZE::DisplayList::~DisplayList() {

}

void RZE::DisplayList::setRenderContext(RenderContext *ctx) {
  current_ctx_ = ctx;
}

int32 RZE::DisplayList::clearVector() {
	command_list.clear();
	return RZE::ErrorCode::kErrorCode_Ok;
}

int32 RZE::DisplayList::clearCommandList() {
	for (int32 i = 0; i < command_list.size(); ++i){
	delete(command_list[i]);
	}
	command_list.clear();
	return RZE::ErrorCode::kErrorCode_Ok;
}

int RZE::DisplayList::vertexBuffer(unsigned int vertex_buffer_int, float *positions, int p_size, float* normals, int n_size, float* uvs, int u_size) {

	if (positions == nullptr) {
    spdlog::get("console")->debug("{}: Null position param", __func__);
		return RZE::ErrorCode::kErrorCode_NullParam;
	}
  if (normals == nullptr) {
    spdlog::get("console")->debug("{}: Null normals param", __func__);
    return RZE::ErrorCode::kErrorCode_NullParam;
  }
  if (uvs == nullptr) {
    spdlog::get("console")->debug("{}: Null uvs param", __func__);
    return RZE::ErrorCode::kErrorCode_NullParam;
  }

	if (p_size == 0) {
    spdlog::get("console")->debug("{}: Null p_size param", __func__);
    return RZE::ErrorCode::kErrorCode_NullParam;
	}
  if (n_size == 0) {
    spdlog::get("console")->debug("{}: Null n_size param", __func__);
    return RZE::ErrorCode::kErrorCode_NullParam;
  }
  if (u_size == 0) {
    spdlog::get("console")->debug("{}: Null u_size param", __func__);
    return RZE::ErrorCode::kErrorCode_NullParam;
  }
	
	Command_VertexBuffer *cvertex = new Command_VertexBuffer();

	cvertex->init(current_ctx_, p_size + n_size + u_size, vertex_buffer_int);
  cvertex->setData(positions, p_size, normals, n_size, uvs, u_size);

	command_list.push_back(cvertex);
	return RZE::ErrorCode::kErrorCode_Ok;
}

int32 RZE::DisplayList::indexBuffer(ushort16 *index, int32 array_size, uint32 index_buffer_int) {
	
	if (index == nullptr) {
    spdlog::get("console")->debug("{}: Null index param", __func__);
		return RZE::ErrorCode::kErrorCode_NullParam;
	}

	if (array_size == 0) {
    spdlog::get("console")->debug("{}: Null array_size param", __func__);
		return 1;
	}

	Command_IndexBuffer *cindex = new Command_IndexBuffer();

	cindex->init(current_ctx_, index, array_size, index_buffer_int);

	command_list.push_back(cindex);
	return RZE::ErrorCode::kErrorCode_Ok;
}

int32 RZE::DisplayList::createMaterial(uint32 material, uint32 vertex_id, uint32 fragment_id) {

	Command_CreateMaterial *cmaterial = new Command_CreateMaterial();

	cmaterial->init(current_ctx_, material, vertex_id, fragment_id);

	command_list.push_back(cmaterial);
	return RZE::ErrorCode::kErrorCode_Ok;
}

int32 RZE::DisplayList::createTexture(const char* filename, uint32 texture, Type type, Parameter parameter, Wrap wrap, Format format, Filter filter) {
	
	Command_createTexture *ctexture = new Command_createTexture();

	ctexture->init(current_ctx_, filename,texture, type, parameter,wrap,format,filter);

	command_list.push_back(ctexture);
	return RZE::ErrorCode::kErrorCode_Ok;
}

int32 RZE::DisplayList::clearWindow(float32 color[4], int32 mask) {

  Command_ClearWindow *cwindow = new Command_ClearWindow();
  
  cwindow->init(color, mask);

  command_list.push_back(cwindow);
  return RZE::ErrorCode::kErrorCode_Ok;
}

int32 RZE::DisplayList::render(uint32 program_int, uint32 vertex_int, uint32 index_int, uint32 node_id, uint32 texture_id) {

	Command_Render *crender = new Command_Render();

	crender->init(current_ctx_, program_int, vertex_int, index_int, node_id, texture_id);

	command_list.push_back(crender);
	return RZE::ErrorCode::kErrorCode_Ok;
}

int32 RZE::DisplayList::executeCommands() {
  for (int32 i = 0; i < command_list.size(); ++i) {
    command_list[i]->execute();
  }
  return RZE::ErrorCode::kErrorCode_Ok;
}

std::vector<RZE::Command*> RZE::DisplayList::getCommandList() {
	return command_list;
}