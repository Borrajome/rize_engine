#include "command_render.h"
#include "deps/GLEW/glew.h"
#include "render_context.h"

#include "mat3x3.hpp"
#include "gtc/type_ptr.hpp"

//#define STB_IMAGE_IMPLEMENTATION
//#include "stb_image.h"
//#include "common_errors.h"

RZE::Command_Render::Command_Render() {

	program_int_ = 0;
	vertex_int_ = 0;
	index_int_ = 0;

}

RZE::Command_Render::~Command_Render() {

}

void RZE::Command_Render::init(RenderContext *ctx, uint32 program_int, uint32 vertex_int, uint32 index_int, uint32 node_id, uint32 texture_id) {
  program_int_ = program_int;
  vertex_int_ = vertex_int;
  index_int_ = index_int;
  node_id_ = node_id;
  texture_id_ = texture_id;

  ctx_ = ctx;
}

void RZE::Command_Render::execute() {

	int32 gpu_vertex_int = ctx_->getInternalBuffer(vertex_int_)->getGPUID();
	int32 gpu_index_int = ctx_->getInternalBuffer(index_int_)->getGPUID();
	int32 gpu_program_int = ctx_->getInternalMaterial(program_int_)->GPUID();

	//GLuint texture;
	//int32 width, height, nr_color_channels;
	//int32 gl_text_unit = 0;

	//const float colors[] = {
	//	1.0f,0.0f,0.0f,1.0f
	//};

	//glGenTextures(1, &texture);
	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, texture);

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


	////glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, colors);
	//stbi_set_flip_vertically_on_load(true);
	//unsigned char *img_data = stbi_load("../resources/textures/Mario_block.png", &width, &height, &nr_color_channels, 0);

	//if (img_data) {
	//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img_data);
	//	glGenerateMipmap(GL_TEXTURE_2D);
	//}
	//else {
	//	spdlog::get("console")->error("Failed to load texture", __func__);
	//}

	//stbi_image_free(img_data);

  ctx_->getInternalBuffer(vertex_int_)->setRender(true);
  ctx_->getInternalBuffer(index_int_)->setRender(true);

  glBindBuffer(GL_ARRAY_BUFFER, gpu_vertex_int);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float32), (void *)0);
  glEnableVertexAttribArray(0);

  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float32), (void *)ctx_->getInternalBuffer(vertex_int_)->posSize());
  glEnableVertexAttribArray(1);

  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float32), (void *)(ctx_->getInternalBuffer(vertex_int_)->posSize() + ctx_->getInternalBuffer(vertex_int_)->norSize()));
  glEnableVertexAttribArray(2);


  int model_matrix_location = glGetUniformLocation(gpu_program_int, "u_m_matrix");
  int view_matrix_location = glGetUniformLocation(gpu_program_int, "u_vp_matrix");
  const float *model_mat = (const float*)glm::value_ptr(ctx_->internal_nodes().models[node_id_]);
  const float *view_mat = (const float*)glm::value_ptr(ctx_->viewMatrix());

  glUseProgram(gpu_program_int);
  glUniformMatrix4fv(model_matrix_location, 1, false, model_mat);
  glUniformMatrix4fv(view_matrix_location, 1, false, view_mat);

  int gl_textUnit = ctx_->getInternalTexture(texture_id_)->textureUnit();
  GLenum type = ctx_->getInternalTexture(texture_id_)->textureType();

  glActiveTexture(GL_TEXTURE0 + gl_textUnit);
  glBindTexture(type, texture_id_);

  int textPos = glGetUniformLocation(gpu_program_int, "block_text");
  glUniform1iv(textPos, 1, &gl_textUnit);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpu_index_int);
  glDrawElements(GL_TRIANGLES, (ctx_->getInternalBuffer(index_int_)->totalSize() / sizeof(ushort16)), GL_UNSIGNED_SHORT, 0);
}