#include "internal_texture.h"


RZE::InternalTexture::InternalTexture() {
	GPUID_ = 0;
	render_ = false;
	init_ = false;
	text_type_ = GL_INVALID_ENUM;
}

RZE::InternalTexture::~InternalTexture() {

}

void RZE::InternalTexture::reset() {
	GPUID_ = 0;
	render_ = false;
	init_ = false;
}

void RZE::InternalTexture::setGPUID(uint32 GPUID) {
	GPUID_ = GPUID;
}


void RZE::InternalTexture::setInit(bool init) {
	init_ = init;
}

void RZE::InternalTexture::setRender(bool render) {
	render_ = render;
}


uint32 RZE::InternalTexture::GPUID() {
	return GPUID_;
}

bool RZE::InternalTexture::hasInitialized() {
	return init_;
}

bool RZE::InternalTexture::hasRendered() {
	return render_;
}

void RZE::InternalTexture::setTextureType(GLenum type) {
	text_type_ = type;
}

void RZE::InternalTexture::setTextureUnit(uint32 text_unit) {
	text_unit_ = text_unit;
}

uint32 RZE::InternalTexture::textureUnit() {
	return text_unit_;
}

GLenum RZE::InternalTexture::textureType() {
	return text_type_;
}

