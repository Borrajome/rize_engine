#include "audio_system.h"
#include "spdlog\spdlog.h"
#include "audio_loader.h"
using namespace RZE;

void Layering::init(Track *track, float delay_time) {

	track_ = track;
	delay_time_ = delay_time;

}

#ifdef FMOD_AUDIO

RZE::AudioSystem::AudioSystem() {

	system_ = nullptr;
	errorCheck(FMOD::System_Create(&system_));
	errorCheck(system_->init(32, FMOD_INIT_NORMAL, 0));
  AudioLoader = new RZE::AudioLoader();
}

RZE::AudioSystem::~AudioSystem() {}


void AudioSystem::update() {

	bool isPlaying = false;

	if (playlist_.size() > 0) {
		if (playlist_[0]->channel_->isPlaying(&isPlaying)) {
			playlist_.erase(playlist_.begin());
			if (playlist_.size() > 0) {
				playChannel(playlist_[0]);
			}
		}
	}

	errorCheck(system_->update());
}

void AudioSystem::shutdown() {
	errorCheck(system_->close());
	errorCheck(system_->release());
}

void AudioSystem::chargeSound(Track *track, const char* filename, bool loop) {

	char *filePath = (char *)calloc(256, sizeof(char));
	strcat(filePath, "../resources/audio/");
	strcat(filePath, filename);

	FMOD::Sound *tempSound = nullptr;

	//errorCheck(system_->createSound(filePath, FMOD_DEFAULT, 0, &tempSound));
  AudioLoader->loadAudio(system_, track, filePath);

	if (!loop) {
		errorCheck(track->sound_->setMode(FMOD_LOOP_OFF));
	}
	else {
		errorCheck(track->sound_->setMode(FMOD_LOOP_NORMAL));
	}

	if (tempSound) {
		track->sound_ = tempSound;
	}
}

void AudioSystem::playSound(Track *track, float volume) {

	FMOD::Channel* tmp_channel = nullptr;

	errorCheck(system_->playSound(track->sound_, 0, false, &tmp_channel));

	if (tmp_channel) {
		errorCheck(tmp_channel->setVolume(volume));
		track->channel_ = tmp_channel;
	}
}

void AudioSystem::stopChannel(Track *track) {

	track->channel_->setPaused(true);
}

void AudioSystem::stopAllChannel() {


	for (int i = 0; i < 16; ++i) {

		Tracks[i].channel_->setPaused(true);
	}

}

void AudioSystem::playChannel(Track *track) {


	bool playing = false;

	if (errorCheck(track->channel_->isPlaying(&playing))) {
		playSound(track, 1.0f);
		setLooping(track, false);
	}

	track->channel_->setPosition(0, FMOD_TIMEUNIT_MS);
	track->channel_->setPaused(false);

}

void AudioSystem::setChannelVolume(Track *track, float volume) {

	track->channel_->setVolume(volume);
}

void AudioSystem::setLooping(Track *track, bool loop) {

	if (loop) {
		track->channel_->setMode(FMOD_LOOP_NORMAL);
	}
	else {
		track->channel_->setMode(FMOD_LOOP_OFF);
	}
}

void AudioSystem::crossfading(Track *track1, Track *track2, int out_seconds, int in_seconds, int delay) {

	int rate;
	int rate2;
	uint64_t a_parentclock;
	uint64_t b_parentclock;

	//FADE OUT
	errorCheck(track1->channel_->getDSPClock(0, &a_parentclock));
	errorCheck(system_->getSoftwareFormat(&rate, 0, 0));
	errorCheck(track1->channel_->addFadePoint(a_parentclock, 1.0f));
	errorCheck(track1->channel_->addFadePoint(a_parentclock + (rate * out_seconds), 0.0f));

	//FADE IN
	errorCheck(track2->channel_->getDSPClock(0, &b_parentclock));
	errorCheck(system_->getSoftwareFormat(&rate2, 0, 0));
	errorCheck(track2->channel_->addFadePoint(b_parentclock, 0.0f));
	errorCheck(track2->channel_->addFadePoint(b_parentclock + (rate2 * in_seconds), 1.0f));
	errorCheck(track2->channel_->setDelay(b_parentclock + (rate2 * delay), 0, false));

}

void AudioSystem::layering(std::vector<Layering>*layer_vec) {

	for (int i = 0; i < layer_vec->size(); i++) {

		playChannel(layer_vec->at(i).track_);

		errorCheck(layer_vec->at(i).track_->channel_->getDSPClock(0, &layer_vec->at(i).clock_));
		errorCheck(system_->getSoftwareFormat(&layer_vec->at(i).rate_, 0, 0));
		errorCheck(layer_vec->at(i).track_->channel_->setDelay(layer_vec->at(i).clock_ + (layer_vec->at(i).rate_ * layer_vec->at(i).delay_time_), 0, false));
	}




}

void AudioSystem::branching(Track *next_track, Track *transition_track) {

	for (int i = 0; i < playlist_.size(); ++i)
		if (playlist_[i]->sound_ == next_track->sound_) return;

	playSound(next_track, 1.0f);
	if (playlist_.size() > 0) {
		setLooping(playlist_[playlist_.size() - 1], false);

		if (playlist_.size() > 1) setLooping(playlist_[playlist_.size() - 2], false);
		stopChannel(next_track);
	}


	setLooping(next_track, true);
	playlist_.push_back(next_track);

	playSound(transition_track, 1.0f);
	stopChannel(transition_track);
	setLooping(transition_track, false);
	playlist_.push_back(transition_track);

}

void AudioSystem::stopBranching() {
	for (int i = 0; i < playlist_.size(); ++i) setLooping(playlist_[i], false);
}


int AudioSystem::errorCheck(FMOD_RESULT result) {

	if (result != FMOD_OK) {
		printf("ERROR FMOD");
		return 1;
	}

	return 0;
}


#else

// Start of SoLoud Implementation

AudioSystem::AudioSystem() {
	system_ = new SoLoud::Soloud();
	system_->init();
  AudioLoader = new RZE::AudioLoader();
}

AudioSystem::~AudioSystem() {

}


void AudioSystem::update() {
	// Free up the tracks that are not being played
	if (playlist_.size() > 0) {
		if (!system_->isValidVoiceHandle(playlist_[0]->channel_)) {
			playlist_.erase(playlist_.begin());
			if (playlist_.size() > 0) {
				playChannel(playlist_[0]);
			}
		}
	}
}

void AudioSystem::shutdown() {
	// Free the tracks' memory and destroy audiosystem itself, if needed
	system_->deinit();
}

void AudioSystem::chargeSound(Track *track, const char *filename, bool loop) {
	char *filePath = (char *)calloc(256, sizeof(char));
	strcat(filePath, "../resources/audio/");
	strcat(filePath, filename);

  AudioLoader->loadAudio(track, filePath);
  track->sound_->setLooping(loop);
}

void AudioSystem::playSound(Track *track, float volume) {

	SoLoud::handle tmp_channel = system_->play(*track->sound_, volume);
	if (tmp_channel) {
		track->channel_ = tmp_channel;
	}
}

void AudioSystem::stopChannel(Track *track) {
	if (track->channel_) {
		system_->setPause(track->channel_, true);
	}
}

void AudioSystem::stopAllChannel() {

	for (int i = 0; i < 16; ++i) {
		system_->setPause(Tracks[i].channel_, true);
	}

}

void AudioSystem::playChannel(Track *track) {
	if (system_->isValidVoiceHandle(track->channel_)) {
		system_->seek(track->channel_, 0.0f);
		system_->setPause(track->channel_, false);
	}
	else {
		playSound(track, 1.0f);
		system_->setLooping(track->channel_, false);
	}
}

void AudioSystem::setChannelVolume(Track *track, float volume) {
	if (track->channel_) {
		system_->setVolume(track->channel_, volume);
	}
}

int AudioSystem::errorCheck(SoLoud::result result) {
	if (result != 0) spdlog::get("console")->debug("{}: SoLoud error - {}", __func__, system_->getErrorString(result));
	return 0;
}

void AudioSystem::crossfading(Track *track1, Track *track2, int out_seconds, int in_seconds, int delay) {

	//Fade out
	//setChannelVolume(track1, 1.0f);
	system_->fadeVolume(track1->channel_, 0.0f, out_seconds);

	//Fade in
	//track2->channel_ = system_->playClocked(delay, *track2->sound_, 0.0f);
	system_->setDelaySamples(track2->channel_, system_->getSamplerate(track2->channel_) * delay);
	system_->fadeVolume(track2->channel_, 1.0f, in_seconds);


}

void AudioSystem::layering(std::vector<Layering>*layer_vec) {
	for (int i = 0; i < layer_vec->size(); i++) {

		playChannel(layer_vec->at(i).track_);
		system_->setDelaySamples(layer_vec->at(i).track_->channel_,
			system_->getSamplerate(layer_vec->at(i).track_->channel_) * layer_vec->at(i).delay_time_);
	}
}

void AudioSystem::branching(Track *next_track, Track *transition_track) {

	for (int i = 0; i < playlist_.size(); ++i)
		if (playlist_[i]->sound_ == next_track->sound_) return;

	playSound(next_track, 1.0f);
	if (playlist_.size() > 0) {
		system_->setLooping(playlist_[playlist_.size() - 1]->channel_, false);
		if (playlist_.size() > 1) system_->setLooping(playlist_[playlist_.size() - 2]->channel_, false);
		stopChannel(next_track);
	}

	system_->setLooping(next_track->channel_, true);
	playlist_.push_back(next_track);

	playSound(transition_track, 1.0f);
	stopChannel(transition_track);
	system_->setLooping(transition_track->channel_, false);
	playlist_.push_back(transition_track);


}

void AudioSystem::stopBranching() {
	for (int i = 0; i < playlist_.size(); ++i) system_->setLooping(playlist_[i]->channel_, false);
}

#endif