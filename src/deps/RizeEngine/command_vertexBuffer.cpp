#include "command_vertexBuffer.h"
#include "deps\GLEW\glew.h"
#include <stdlib.h>
#include <string.h>
#include "render_context.h"
#include "spdlog\spdlog.h"

RZE::Command_VertexBuffer::Command_VertexBuffer() {
	vertices_ = nullptr;
	array_size_ = 0;
	vertex_buffer_int_ = 0;
}

RZE::Command_VertexBuffer::~Command_VertexBuffer() {}

void RZE::Command_VertexBuffer::init(RenderContext *ctx, int32 total_byte_size, uint32 vertex_buffer_int){

	vertices_ = (float32*)malloc(sizeof(float32) * total_byte_size);

	if (vertices_ == nullptr) {
    spdlog::get("console")->debug("{}: Command_VertexBuffer MemoryAlloc error", __func__);
		return;
	}

	array_size_ = total_byte_size;
	vertex_buffer_int_ = vertex_buffer_int;
	ctx_ = ctx;
} 

void RZE::Command_VertexBuffer::setData(const float32 *positions, int32 p_size, const float32 *normals, int32 n_size, const float32 *uvs, int32 u_size){
  
  positions_size_ = p_size;
  normals_size_ = n_size;
  uvs_size_ = u_size;


  memcpy(vertices_, positions, positions_size_);
  if (n_size > 0 && normals != nullptr) memcpy(vertices_ + positions_size_/4, normals, normals_size_);
  if(u_size > 0 && uvs != nullptr) memcpy(vertices_ + positions_size_/4 + normals_size_/4, uvs, uvs_size_);
}

void RZE::Command_VertexBuffer::execute() {

  if (ctx_->getInternalBuffer(vertex_buffer_int_)->getInit() == true && 
    ctx_->getInternalBuffer(vertex_buffer_int_)->getRender() == false) {

	  uint32 GPU_id = 0;
	  glGenBuffers(1, &GPU_id);

    ctx_->getInternalBuffer(vertex_buffer_int_)->setGPUID(GPU_id);
    ctx_->getInternalBuffer(vertex_buffer_int_)->setSizes(positions_size_, normals_size_, uvs_size_);

	  glBindBuffer(GL_ARRAY_BUFFER, GPU_id);
	  glBufferData(GL_ARRAY_BUFFER, array_size_, vertices_, GL_DYNAMIC_DRAW);

  }
  else {
    glBindBuffer(GL_ARRAY_BUFFER, ctx_->getInternalBuffer(vertex_buffer_int_)->getGPUID());
    glBufferSubData(GL_ARRAY_BUFFER, 0, array_size_, vertices_);
  }
}
