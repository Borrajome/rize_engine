#include "audio_spectrum_visualizer.h"
#include "stdlib.h"
#include "common_errors.h"
#include "spdlog\spdlog.h"
#include "audio_system.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"



RZE::AudioSpectrumVisualizer::AudioSpectrumVisualizer() {

}

RZE::AudioSpectrumVisualizer::~AudioSpectrumVisualizer() {

}

int32 RZE::AudioSpectrumVisualizer::GenerateSpectrumImage(char* audio_path, AudioLoader* audio_loader, RGBA_color back_color, RGBA_color graph_color){

  if (audio_path == "") return ErrorCode::kErrorCode_NullParam;
  if(audio_loader == nullptr) return ErrorCode::kErrorCode_NullParam;

  // Full route to the audio
  char *filePath = (char *)calloc(256, sizeof(char));
  strcat(filePath, "../resources/audio/");
  strcat(filePath, audio_path);
  
  //Getting the Raw Data of the audio 
  RZE::RawAudio *audio = nullptr;
  audio = audio_loader->audioInfo(filePath);
  if(audio == nullptr) return ErrorCode::kErrorCode_NullParam;
  
  int column_size = 1;
  int number_of_lines = (1000 / (column_size * 2));
  
  uint32 *samples_median = (uint32*)malloc(sizeof(uint32) * number_of_lines);
  
  // The samples are divided into equal parts. We use only one channel at the moment, so we divide it too.
  float sample_part = (static_cast<float>(audio->sample_count / audio->channels) / number_of_lines);
  float acc = 0.0f;

  // This ifdef is needed because of the different raw data formats needed for each libary.
  // FMOD uses the data as is loaded by dr_, with stride between channels: 00112233
  // SoLoud uses the data as entire blocks for each channel: 01230123
  // Because of this, to limit ourselves to the data of the first channel, we move through the pointer differently.
  #ifdef FMOD_AUDIO
  for (int i = 0; i < number_of_lines; ++i) {
    acc = 0.0f;
    for (int j = 0; j < sample_part; ++j) acc += abs(audio->data[j*audio->channels + i * audio->channels * static_cast<int>(sample_part)]);
    samples_median[i] = (acc / sample_part) * 150;
  }
  #else
  for (int i = 0; i < number_of_lines; ++i) {
    acc = 0.0f;
    for (int j = sample_part * i; j < sample_part * (i + 1); ++j) acc += abs(audio->data[j]);
    samples_median[i] = (acc / sample_part) * 150;
  }
  #endif

  // Clamp to max height to avoid stepping out of the buffer
  for(int i = 0; i < number_of_lines; ++i){
    if(samples_median[i] > 148) samples_median[i] = 148;
  }

  char* image_data = (char*)malloc(sizeof(char) * 4 * 1000 * 300);

  // Initial, background color
  for(uint64 i = 0; i < 1000 * 300; ++i){
    image_data[i * 4 + 0] = back_color.R;
    image_data[i * 4 + 1] = back_color.G;
    image_data[i * 4 + 2] = back_color.B;
    image_data[i * 4 + 3] = back_color.A;
  }

  // Printing of the lines: the median values are used as height
  for (int x = 0; x < 1000; x+= (column_size*2)) {
    for (int y = 150-samples_median[x / (column_size * 2)] - 1; y < 150 + samples_median[x / (column_size * 2)] + 1; ++y) {
      for (int j = 0; j < column_size; ++j) {
        image_data[(j + x + y * 1000) * 4 + 0] = graph_color.R;
        image_data[(j + x + y * 1000) * 4 + 1] = graph_color.G;
        image_data[(j + x + y * 1000) * 4 + 2] = graph_color.B;
        image_data[(j + x + y * 1000) * 4 + 3] = graph_color.A;
      }
    }
  }
  
  free(samples_median);

  char imagePath[256];
  char imageName[256];

  // Looking for the first dot in the name file (subject to change)
  int dot = 0;
  while (audio_path[dot] != '.') dot++;
  for(int i = 0; i < dot; ++i) imageName[i] = audio_path[i];
  imageName[dot] = '\0';

  // parsing for the complete destination route
  for(int i=0; i<256; ++i) imagePath[i] = '\0';
  strcat(imagePath, "../resources/images/");
  strcat(imagePath, imageName);
  strcat(imagePath, ".png");

  // Writing and storing the image
  stbi_write_png(imagePath, 1000, 300, 4, image_data, 1000*4);
  free(image_data);

  return ErrorCode::kErrorCode_Ok;
}