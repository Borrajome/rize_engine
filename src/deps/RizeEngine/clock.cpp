#include "clock.h"



using namespace RZE;


RZE::Clock::Clock() {
	start();
}

RZE::Clock::~Clock() {}

RZE::Clock::Clock(const Clock &other) {
	
}


void Clock::start() {
	QueryPerformanceCounter(&startTime);
}

void Clock::stop() {
	QueryPerformanceCounter(&endTime);
}

double Clock::ellapsedMS() {

	LARGE_INTEGER frequency;

	QueryPerformanceFrequency(&frequency);

	if (endTime.QuadPart == 0) {
		stop();
	}

	double elapsed_time = endTime.QuadPart - startTime.QuadPart;

	elapsed_time = elapsed_time * 1;


	// return the time in milliseconds
	return elapsed_time / (double)frequency.QuadPart;
}

