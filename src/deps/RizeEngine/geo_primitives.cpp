#include <gtc/matrix_transform.hpp>
#include "geo_primitives.h"
#include <cmath>

RZE::RenderObjectIndexes RZE::GeoPrimitives::RenderCube(RZE::RenderContext *ctx, RZE::DisplayList *dl, RZE::kShaderType shader_type, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation_axis, float32 angle) {
	RZE::RenderObjectIndexes indexes;

	indexes.vertex_id = ctx->createBuffer(sizeof(float32) * (16 * 3 + 8 * 2));
	indexes.index_id = ctx->createBuffer(sizeof(ushort16) * 36);
  // Should be mapped to the corresponding internal buffer
	indexes.material_id = static_cast<uint32>(shader_type);

	float32 positions[] = {
		//-1.0f,  1.0f,  -1.0f,	// left up 
		//1.0f,  1.0f,  -1.0f,	// right up
		//-1.0f,  -1.0f,  -1.0f,	// left down 
		//1.0f,  -1.0f,  -1.0f,	// right down 
		//-1.0f,  1.0f,  1.0f,
		//1.0f,  1.0f,  1.0f,
		//-1.0f,  -1.0f,  1.0f,
		//1.0f,  -1.0f,  1.0f,

		-1.0f,  1.0f,  1.0f,  // Vertex 0 front upper-left
		 -1.0f, -1.0f,  1.0f,  // Vertex 1 front down-left
		 1.0f, -1.0f,  1.0f,  // Vertex 2 front down-right
		 1.0f,  1.0f,  1.0f,  // Vertex 3 front up-right

		 -1.0f,  1.0f, -1.0f,  // Vertex 4 back upper-left
		 -1.0f, -1.0f, -1.0f,  // Vertex 5 back down-left
		 1.0f, -1.0f, -1.0f,  // Vertex 6 back down-right
		 1.0f,  1.0f, -1.0f,  // Vertex 7 back up-right

		 1.0f,  1.0f,  1.0f,  // Vertex 8 right upper-left
		 1.0f, -1.0f,  1.0f,  // Vertex 9 right down-left
		 1.0f, -1.0f, -1.0f,  // Vertex 10 right down-right
		 1.0f,  1.0f, -1.0f,  // Vertex 11 right up-right

		 -1.0f,  1.0f, -1.0f,  // Vertex 12 left upper-left
		 -1.0f, -1.0f, -1.0f,  // Vertex 13 left down-left
		 -1.0f, -1.0f,  1.0f,  // Vertex 14 left down-right
		 -1.0f,  1.0f,  1.0f,  // Vertex 15 left up-right

		 -1.0f,  1.0f, -1.0f,  // Vertex 16 top upper-left
		 -1.0f,  1.0f,  1.0f,  // Vertex 17 top down-left
		 1.0f,  1.0f,  1.0f,  // Vertex 18 top down-right
		 1.0f,  1.0f, -1.0f,  // Vertex 19 top up-right

		 -1.0f, -1.0f,  1.0f,  // Vertex 20 down upper-left
		 -1.0f, -1.0f, -1.0f,  // Vertex 21 down down-left
		 1.0f, -1.0f, -1.0f,  // Vertex 22 down down-right
		 1.0f, -1.0f,  1.0f,  // Vertex 23 down up-right
	};

	float32 normals[] = {
		-1.0f,  1.0f, -1.0f, // left up 
		 1.0f,  1.0f, -1.0f, // right up 
		-1.0f, -1.0f, -1.0f, // left down 
		 1.0f, -1.0f, -1.0f, // right down
		-1.0f,  1.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,
		 1.0f, -1.0f,  1.0f,
	};

	float32 uvs[] = {
		//0.0f, 1.0f, // left up 
		//0.0f, 1.0f, // right up 
		//0.0f, 0.0f, // left down 
		//1.0f, 0.0f, // right down
		//0.0f, 0.0f,
		//1.0f, 0.0f,
		//0.0f, 1.0f,
		//1.0f, 1.0f,

		 0.0, 1.0,
		 0.0, 0.0,
		 1.0, 0.0,
		 1.0, 1.0,

		 1.0, 1.0,
		 1.0, 0.0,
		 0.0, 0.0,
		 0.0, 1.0,

		 0.0, 1.0,
		 0.0, 0.0,
		 1.0, 0.0,
		 1.0, 1.0,

		 0.0, 1.0,
		 0.0, 0.0,
		 1.0, 0.0,
		 1.0, 1.0,

		 0.0, 1.0,
		 0.0, 0.0,
		 1.0, 0.0,
		 1.0, 1.0,

		 0.0, 1.0,
		 0.0, 0.0,
		 1.0, 0.0,
		 1.0, 1.0
	};

	ushort16 indices[] = {
		//0,1,2, // front
		//1,3,2,

		//5,4,6, // back
		//7,5,6,

		//4,0,6, // left
		//6,0,2,

		//1,5,3, // right
		//5,7,3,

		//2,3,6, // up
		//3,7,6,

		//0,4,5, // down
		//5,1,0,

		 0, 1, 2,
		 0, 2, 3,

		 7, 6, 5,
		 7, 5, 4,

		 8, 9, 10,
		 8, 10, 11,

		 12, 13, 14,
		 12, 14, 15,

		 16, 17, 18,
		 16, 18, 19,

		 20, 21, 22,
		 20, 22, 23
	};

	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, position);
	model = glm::rotate(model, angle, rotation_axis);
	model = glm::scale(model, scale);

	dl->vertexBuffer(indexes.vertex_id, positions, sizeof(positions),
		normals, sizeof(normals), uvs, sizeof(uvs));
	dl->indexBuffer(indices, sizeof(indices), indexes.index_id);

	return indexes;
}

RZE::RenderObjectIndexes RZE::GeoPrimitives::RenderPiramid(RenderContext *ctx, DisplayList *dl, RZE::kShaderType shader_type, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation_axis, float32 angle) {

	RZE::RenderObjectIndexes indexes;

	float32 positions[] = {
		0.0f,  1.0f, 0.0f,   // front
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		1.0f,-1.0f,-1.0f,       // down 1
		-1.0f,-1.0f,-1.0f,
		1.0f, -1.0f, 1.0f,


		-1.0f,-1.0f,-1.0f,      // down 2
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,


		1.0f,-1.0f,-1.0f,     // right
		0.0f,1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,

		0.0f,1.0f,0.0f,    // back
		1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,

		-1.0f,-1.0f,1.0f,   // left
		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,-1.0f,



	};

	float32 normals[] = {
		0.0f, 1.0f, 0.0f,
	   -1.0f, 0.0f,1.0f,
		1.0f, 0.0f, 1.0f,

		1.0f,-1.0f,-1.0f,
		-1.0f,0.0f,-1.0f,
		1.0f, 0.0f, 1.0f,

		-1.0f,1.0f,-1.0f,
		-1.0f, 0.0f,1.0f,
		1.0f, 0.0f, 1.0f,

		1.0f,-1.0f,-1.0f,
		0.0f, 1.0f, 0.0f,
		1.0f, 0.0f, 1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f,-1.0f,-1.0f,
		-1.0f,1.0f,-1.0f,

		-1.0f, 0.0f,1.0f,
		0.0f, 1.0f, 0.0f,
		-1.0f,1.0f,-1.0f,
	};

	float32 uvs[] = {
		0.5f, 1.0f,   // top-center corner
		0.0f, 0.0f,   // lower-left corner  
		1.0f, 0.0f,   // lower-right corner
		
		1.0f, 1.0f,   
		0.0f, 1.0f,  
		1.0f, 0.0f,

		0.0f, 1.0f,   
		0.0f, 0.0f, 
		1.0f, 0.0f,

		1.0f, 0.0f,
		0.5f, 1.0f,
		1.0f, 0.0f,

		0.5f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,

		0.0f,0.0f,
		0.5f,1.0f,
		0.0f,1.0f
	};

	ushort16 indices[] = {
		0,1,2,
		2,4,3,
		2,1,4,
		3,0,2,
		0,3,4,
		1,0,4
	};

	indexes.vertex_id = ctx->createBuffer(sizeof(positions));
	indexes.index_id = ctx->createBuffer(sizeof(indices));
  // Should be mapped to the corresponding internal buffer	
  indexes.material_id = static_cast<uint32>(shader_type);

	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, position);
	model = glm::rotate(model, angle, rotation_axis);
	model = glm::scale(model, scale);

	dl->vertexBuffer(indexes.vertex_id, positions, sizeof(positions),
		normals, sizeof(normals), uvs, sizeof(uvs));
	dl->indexBuffer(indices, sizeof(indices), indexes.index_id);

	return indexes;
}

RZE::RenderObjectIndexes RZE::GeoPrimitives::RenderSphere(RenderContext *ctx, DisplayList *dl, float radius, int rings, int sectors, RZE::kShaderType shader_type, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation_axis, float32 angle) {

	RZE::RenderObjectIndexes indexes;

	std::vector<float>positions;
	std::vector<ushort16>indices;

	float const R = 1. / (float)(rings - 1);
	float const S = 1. / (float)(sectors - 1);
	int r, s;

	positions.resize(rings * sectors * 3);

	std::vector<float>::iterator v = positions.begin();

	for (r = 0; r < rings; r++) for (s = 0; s < sectors; s++) {
		float const y = sin(-(3.1415/2) + 3.1415 * r * R);
		float const x = cos(2 * 3.1415 * s * S) * sin(3.1415 * r * R);
		float const z = sin(2 * 3.1415 * s * S) * sin(3.1415 * r * R);


		*v++ = x * radius;
		*v++ = y * radius;
		*v++ = z * radius;

	}

	indices.resize(rings * sectors * 4);

	std::vector<ushort16>::iterator i = indices.begin();

	for (r = 0; r < rings; r++) for (s = 0; s < sectors; s++) {
		*i++ = r * sectors + s;
		*i++ = r * sectors + (s + 1);
		*i++ = (r + 1) * sectors + (s + 1);
		*i++ = (r + 1) * sectors + s;
	}

	indexes.vertex_id = ctx->createBuffer(positions.size() * sizeof(float));
	indexes.index_id = ctx->createBuffer(indices.size() * sizeof(int));
	// Should be mapped to the corresponding internal buffer	
	indexes.material_id = static_cast<uint32>(shader_type);

	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, position);
	model = glm::rotate(model, angle, rotation_axis);
	model = glm::scale(model, scale);

	dl->vertexBuffer(indexes.vertex_id, &positions[0], positions.size() * sizeof(float),
		0, 0, 0, 0);
	dl->indexBuffer(&indices[0], indices.size() * sizeof(ushort16), indexes.index_id);

	return indexes;
}

	

