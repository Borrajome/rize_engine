#include "internalbuffer.h"


RZE::InternalBuffer::InternalBuffer() {

	GPUID_ = 0;
	render_ = false;
	init_ = false;

  size_ = 0;
  total_size_ = 0;
  positions_size_ = 0;
  normals_size_ = 0;
  uvs_size_ = 0;

}

RZE::InternalBuffer::~InternalBuffer() {

}

void RZE::InternalBuffer::init(uint32 size) {

	total_size_ = size;
	init_ = true;
}

void RZE::InternalBuffer::reset() {
	GPUID_ = 0;
	render_ = false;
	init_ = false;

  size_ = 0;
	total_size_ = 0;
  positions_size_ = 0;
  normals_size_ = 0;
  uvs_size_ = 0;
}

void RZE::InternalBuffer::setSizes(uint32 positions_size, uint32 normals_size, uint32 uvs_size) {
  positions_size_ = positions_size;
  normals_size_ = normals_size;
  uvs_size_ = uvs_size;
}

uint32 RZE::InternalBuffer::totalSize() {
	return total_size_;
}
uint32 RZE::InternalBuffer::posSize() {
	return positions_size_;
}
uint32 RZE::InternalBuffer::norSize() {
	return normals_size_;
}
uint32 RZE::InternalBuffer::uSize() {
	return uvs_size_;
}

void RZE::InternalBuffer::setGPUID(uint32 GPUID) {

	GPUID_ = GPUID;

}

uint32 RZE::InternalBuffer::getGPUID() {

  return GPUID_;

}

bool RZE::InternalBuffer::getRender() {
	return render_;
}

bool RZE::InternalBuffer::getInit() {
	return init_;
}

void RZE::InternalBuffer::setInit(bool init) {
	init_ = init;
}

void RZE::InternalBuffer::setRender(bool render) {
  render_ = render;
}