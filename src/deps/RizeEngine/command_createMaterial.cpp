#include "command_createMaterial.h"
#include "deps/GLEW/glew.h"
#include <stdio.h>
#include "render_context.h"
#include <gtc/matrix_transform.hpp>



RZE::Command_CreateMaterial::Command_CreateMaterial() {

	material_id_ = 0;

}

RZE::Command_CreateMaterial::~Command_CreateMaterial() {

}

void RZE::Command_CreateMaterial::init(RenderContext *ctx, uint32 material_id, uint32 vertex_id, uint32 fragment_id) {

  material_id_ = material_id;
  ctx_ = ctx;
  vertex_id_ = vertex_id;
  fragment_id_ = fragment_id;
  
}




void RZE::Command_CreateMaterial::execute(){


	uint32 program_id; 

	program_id = glCreateProgram();
	ctx_->getInternalMaterial(material_id_)->setGPUID(program_id);
	ctx_->getInternalMaterial(material_id_)->setInit(true);

	glAttachShader(program_id, ctx_->get_vertex_shader_id(vertex_id_));
	glAttachShader(program_id, ctx_->get_fragment_shader_id(fragment_id_));
	glLinkProgram(program_id);

	//glDeleteShader(ctx_->get_vertex_shader_id(vertex_id_));
	//glDeleteShader(ctx_->get_fragment_shader_id(fragment_id_));

}









	