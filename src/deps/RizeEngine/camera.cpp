#include "camera.h"
#include "glfw3.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "spdlog\spdlog.h"

RZE::Camera::Camera() {
  speed_ = 2.0f;
  rot_speed_ = 50.0f;
  sensitivity_ = 0.05f;

  camera_position_ = glm::vec3(0.0f,0.0f,3.0f);
  camera_up_ = glm::vec3(0.0f, 0.0f, 0.0f);

  yaw_ = -90.0f;
  pitch_ = 0.0f;

  previous_mouse_x_position_ = 0.0f;
  previous_mouse_y_position_ = 0.0f;

  camera_control_key_ = RZE::InputKey::KINPUT_KEY_LEFT_SHIFT;
  mouse_controlling_camera_ = false;

  updateVectors();
}

RZE::Camera::~Camera() {

}

void RZE::Camera::init(glm::vec3 camera_position, glm::vec3 camera_up, float32 yaw, float32 pitch) {
  camera_position_ = camera_position;
  camera_up_ = camera_up;

  yaw_ = yaw;
  pitch_ = pitch;

  updateVectors();
}

void RZE::Camera::changeSpeed(float32 speed, float32 rotation_speed, float32 mouse_s) {
  speed_ = speed;
  rot_speed_ = rotation_speed;
  sensitivity_ = mouse_s;
}

void RZE::Camera::setCameraControlKey(InputKey key) {
  camera_control_key_ = key;
}


void RZE::Camera::keyboardInput(Window *win) {

  if (!mouse_controlling_camera_) return;

  up_ = win->getKey(InputKey::KINPUT_KEY_W);
  down_ = win->getKey(InputKey::KINPUT_KEY_S);
  right_ = win->getKey(InputKey::KINPUT_KEY_D);
  left_ = win->getKey(InputKey::KINPUT_KEY_A);
  key_y_rotation_ = win->getKey(InputKey::KINPUT_KEY_UP);
  key_y_rotation_ = win->getKey(InputKey::KINPUT_KEY_DOWN);
  key_x_rotation_ = win->getKey(InputKey::KINPUT_KEY_RIGHT);
  key_x_rotation_ = win->getKey(InputKey::KINPUT_KEY_LEFT);
}

void RZE::Camera::mouseInput(Window *win, bool reset_mouse) {

  if (win->getKey(camera_control_key_)){
   mouse_controlling_camera_ = true;
  }
  else{
    mouse_controlling_camera_ = false;
  }

  if (win->getKeyDown(camera_control_key_) && reset_mouse) {
    win->setMousePosition(win->width() / 2, win->height() / 2);
    win->captureMouse(true);
  }

  if (win->getKeyUp(camera_control_key_) && reset_mouse) {
    win->captureMouse(false);
  }

  if(!mouse_controlling_camera_) return;

  float64 x_pos, y_pos;
  win->mousePosition(&x_pos, &y_pos);

  glm::clamp<int32>(x_pos, 0, win->width());
  glm::clamp<int32>(y_pos, 0, win->height());


  if(reset_mouse){

    int32 scr_width_h = win->width() / 2;
    int32 scr_height_h = win->height() / 2;

    mouse_x_rotation_ = x_pos - scr_width_h;
    mouse_y_rotation_ = y_pos - scr_height_h;
    win->setMousePosition(scr_width_h, scr_width_h);
    
  }
  else {
  
    if (win->getKeyDown(camera_control_key_)) {
      previous_mouse_x_position_ = x_pos;
      previous_mouse_y_position_ = y_pos;
    }

    mouse_x_rotation_ = x_pos - previous_mouse_x_position_;
    mouse_y_rotation_ = y_pos - previous_mouse_y_position_;
    previous_mouse_x_position_ = x_pos;
    previous_mouse_y_position_ = y_pos;
    win->captureMouse(false);


    int32 new_x_pos = 0, new_y_pos = 0;

    if (previous_mouse_x_position_ >= win->width()) new_x_pos = 10;
    else if(previous_mouse_x_position_ <= 0) new_x_pos = win->width() - 10;
    else new_x_pos = previous_mouse_x_position_;

    if (previous_mouse_y_position_ >= win->height()) new_y_pos = 10;
    else if (previous_mouse_y_position_ <= 0) new_y_pos = win->height() - 10;
    else new_y_pos = previous_mouse_y_position_;

    if(new_x_pos != previous_mouse_x_position_ || new_y_pos != previous_mouse_y_position_){
      win->setMousePosition(new_x_pos, new_y_pos);
      previous_mouse_x_position_ = new_x_pos;
      previous_mouse_y_position_ = new_y_pos;
    }
  }
}


void RZE::Camera::resetInput() {
  up_ = false;
  down_ = false;
  left_ = false;
  right_ = false;

  key_x_rotation_ = 0;
  key_y_rotation_ = 0;

  mouse_x_rotation_ = 0.0f;
  mouse_y_rotation_ = 0.0f;
}

void RZE::Camera::logic(float32 delta_time) {
  
  float32 mov_speed = speed_ * delta_time;
  if (up_)
    camera_position_ += camera_front_ * mov_speed;
  if (down_)
    camera_position_ -= camera_front_ * mov_speed;
  if (left_)
    camera_position_ -= camera_right_ * mov_speed;
  if (right_)
    camera_position_ += camera_right_ * mov_speed;

  
  if (key_x_rotation_ != 0) {
    yaw_ += key_x_rotation_ * delta_time * rot_speed_;
  }
  if (key_y_rotation_ != 0) {
    pitch_ += key_y_rotation_ * delta_time * rot_speed_;
  }

  yaw_ += mouse_x_rotation_ * sensitivity_;
  pitch_ -= mouse_y_rotation_ * sensitivity_;

  // This fps camera clamps the y axis to evade gimball lock and camera flip
  if (pitch_ > 89.0f)
    pitch_ = 89.0f;
  if (pitch_ < -89.0f)
    pitch_ = -89.0f;

  if (key_x_rotation_ || key_y_rotation_ || mouse_x_rotation_ || mouse_y_rotation_ ||
    up_ || down_ || left_ || right_) {
      updateVectors();
  }
  
  //spdlog::get("console")->debug("Camera pos: x = {}, y = {}, z = {};", camera_position_.x, camera_position_.y, camera_position_.z);

  resetInput();
}

void RZE::Camera::mouseMode(Window *win, bool mouse_control) {
  win->captureMouse(mouse_control);
}


glm::mat4 RZE::Camera::getViewMatrix() {
  return glm::lookAt(camera_position_, camera_position_ + camera_front_, camera_up_);
}

glm::mat4 RZE::Camera::getViewProjectionMatrix(float32 fov, float32 width, float32 height, float32 near_plane, float32 far_plane) {
  return  glm::perspective(glm::radians(fov), width / height, near_plane, far_plane) * glm::lookAt(camera_position_, camera_position_ + camera_front_, camera_up_);
}

void RZE::Camera::updateVectors() {

  glm::vec3 front;
  front.x = cos(glm::radians(yaw_)) * cos(glm::radians(pitch_));
  front.y = sin(glm::radians(pitch_));
  front.z = sin(glm::radians(yaw_)) * cos(glm::radians(pitch_));
  camera_front_ = glm::normalize(front);

  // Also re-calculate the Right and Up vector
  camera_right_ = glm::normalize(glm::cross(camera_front_, glm::vec3(0.0f, 1.0f, 0.0f)));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
  camera_up_ = glm::normalize(glm::cross(camera_right_, camera_front_));
}


