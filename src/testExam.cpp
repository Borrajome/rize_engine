#define PX_SCHED_IMPLEMENTATION 1
#include "deps/PX/px_sched.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <stdio.h>
#include <conio.h>
#include <chrono>
#include <vector>
#include <string>
#include <iostream>
#include <time.h>
#include "common_errors.h"

#include "obj_loader.h"

#include "platform_types.h"
#include "render_context.h"
#include "window.h"
#include "glfw3.h"
#include "imgui_window.h"
#include "clock.h"
#include "display_list.h"
#include "camera.h"
#include "geo_primitives.h"
#include <gtc/matrix_transform.hpp>
#include "minitrace/minitrace.h"
#include "Lua/lua.hpp"
#include "node.h"

RZE::RenderContext *render_ctx;

RZE::Camera camera;

void logic(RZE::Window *main_window) {

	float32 color[4] = { 0.5f, 0.5f, 0.5f, 1.0f };
	int32 mask = RZE::ClearBit::kClearBit_Color | RZE::ClearBit::kClearBit_Depth;

	RZE::DisplayList *logic_dl = new RZE::DisplayList();

	logic_dl->setRenderContext(render_ctx);

	RZE::Node parent_node = RZE::Node::parent_node(render_ctx);
	RZE::Node sun = parent_node.create_child();
	RZE::Node mars = parent_node.create_child();
	RZE::Node moon = parent_node.create_child();

	sun.set_position(0.0f, 0.0f, 0.0f);
	sun.set_rotation(0.0f, 1.0f, 0.0f, 0.0f);
	sun.set_scale(0.15f, 0.25f, 0.15f);

	//mars.set_position(0.0f, 0.0f, -2.0f);
	//mars.set_rotation(0.0f, 1.0f, 0.0f, 0.0f);
	//mars.set_scale(0.15f, 0.25f, 0.15f);

	//moon.set_position(3.0f, 0.0f, -3.0f);
	//moon.set_rotation(0.0f, 1.0f, 0.0f, 0.0f);
	//moon.set_scale(0.15f, 0.25f, 0.15f);

	logic_dl->createMaterial(render_ctx->material_ids_[0], 0, 0);


	RZE::RenderObjectIndexes cube = RZE::GeoPrimitives::RenderPiramid(render_ctx, logic_dl, RZE::kShaderType::kShaderTypeRed, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f);


	//RZE::RenderObjectIndexes cube = RZE::GeoPrimitives::RenderSphere(render_ctx, logic_dl,1.0f,12,24, RZE::kShaderType::kShaderTypeRed, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f);

	sun.set_geo_indexes(cube);
	/*mars.set_geo_indexes(cube);
	moon.set_geo_indexes(cube);*/

	camera.changeSpeed(2.0f, 50.0f, 0.10f);

	float rot = 0.0f;

	while (main_window->processEvents()) {
		logic_dl->clearWindow(color, mask);

		camera.mouseInput(main_window, false);
		camera.keyboardInput(main_window);
		camera.logic(0.1);

		parent_node.set_rotation(0.0f, 1.0f, 0.0f, rot);
		sun.set_rotation(0.0f, 1.0f, 0.0f, rot * 2);
		/*mars.set_rotation(0.0f, 1.0f, 0.0f, rot * 3);
		moon.set_rotation(0.0f, 1.0f, 0.0f, rot * 5);*/

		render_ctx->CalculateLocalModels();
		render_ctx->CalculateNestedModels();
		render_ctx->RenderNodes(logic_dl);

		
		render_ctx->copyDisplayList(logic_dl);
		logic_dl->clearVector();

		rot += 0.01f;
	}

}


int main() {

	spdlog::init_thread_pool(8192, 1);
	spdlog::flush_every(std::chrono::seconds(5));
	spdlog::get("console") = spdlog::create_async_nb<spdlog::sinks::stdout_color_sink_mt>("console");
	spdlog::get("console")->info("Asyncronous console logger ready");

	RZE::Window main_window;
	main_window.initWindow(1920, 1080);

	render_ctx = new RZE::RenderContext();

	px_sched::Scheduler schd;
	px_sched::SchedulerParams schd_params;

	schd_params.num_threads = 8;
	schd_params.max_running_threads = 8;
	schd.init(schd_params);

	auto logic_job = [&main_window] { logic(&main_window); };
	schd.run(logic_job);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	while (main_window.processEvents()) {
		main_window.InputService();
		render_ctx->updateViewMatrix(camera.getViewProjectionMatrix(70.0f, 1280.0f, 1280.0f, 0.1f, 100.0f));

		render_ctx->render();
		main_window.swap();
	}



	
	//Preventing bug with async log and VS
	spdlog::shutdown();
	main_window.shutdownWindow();


	return 0;
}