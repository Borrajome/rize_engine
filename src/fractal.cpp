#define PX_SCHED_IMPLEMENTATION 1
#include "deps/PX/px_sched.h"

#include <thread>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdlib.h>

#include "deps/RizeEngine/clock.h"
#include "window.h"
#include "linmath.h"

using namespace std;

const int width = 256;
const int height = 256;

int maxIteration = 100;

double minRealValue = -1.5;
double maxRealValue = 1.0;

double minImaValue = -2.0;
double maxImaValue = 2.0;

int brightnes = 256;

px_sched::Scheduler schd;
px_sched::Sync s1;

GLuint quad_vertices_ID;
GLuint quad_indices_ID;
GLuint quad_vertex_shader_ID;
GLuint quad_fragment_shader_ID;
GLuint quad_program_ID;
unsigned int texture;




int findIterations(double cr, double ci, int max_iterations) {

  int i = 0;
  double zr = 0.0;
  double zi = 0.0;

  while (i < max_iterations && zr * zr + zi * zi < 16.0) {

    double temp = zr * zr - zi * zi + cr;
    zi = 2.0 * zr * zi + ci;
    zr = temp;
    i++;
  }
  return i;
}


void ImageMandelbrot() {

  //Open the output file, write the PPM header..
  ofstream fout("../examples/Fractals/mandelbrot_image.ppm");

  fout << "P3" << endl; // "Magic Number" - PPM file
  fout << width << " " << height << endl; // Dimensions
  fout << "256" << endl; // Max value of a pixel RGB value ...

                         // For every pixel.. 

  for (int y = 0; y < height; y++)
  {
    for (int x = 0; x < width; x++)
    {
      // Find the real and imaginary value for c, corresponding to that x, y pixel in the image
      double cr = x * ((maxRealValue - minRealValue) / width) + minRealValue;
      double ci = y * ((maxImaValue - minImaValue) / height) + minImaValue;

      // Find the number of iterations in the Mandelbrot formula using said c

      int n = findIterations(cr, ci, maxIteration);

      // Map the resulting number to an RGB value
      int r = (n % brightnes);
      int g = ((n * 3) % brightnes);
      int b = (n % brightnes);

      // Output it to the image
      fout << r << " " << g << " " << b << " ";

    }
    fout << endl;
  }
  fout.close();
  cout << "Finished!" << endl;


}

void DataMandelbrot(unsigned char *data) {

  for (int y = 0; y < height; y++)
  {
    for (int x = 0; x < width; x++)
    {
      // Find the real and imaginary value for c, corresponding to that x, y pixel in the image
      double cr = x * ((maxRealValue - minRealValue) / width) + minRealValue;
      double ci = y * ((maxImaValue - minImaValue) / height) + minImaValue;

      // Find the number of iterations in the Mandelbrot formula using said c

      int n = findIterations(cr, ci, maxIteration);

      // Map the resulting number to an RGB value
      int r = (n % brightnes);
      int g = ((n * 3) % brightnes);
      int b = (n % brightnes);
      
      // Output it to the data
      *(data + (x + y * width) * 3 + 0) = r;
      *(data + (x + y * width) * 3 + 1) = g;
      *(data + (x + y * width) * 3 + 2) = b;


    }
  }

}

void ImageJulia() {

  // Open the output file, write the PPM header..
  ofstream fout("../examples/Fractals/julia_image.ppm");

  fout << "P3" << endl; // "Magic Number" - PPM file
  fout << width << " " << height << endl; // Dimensions
  fout << "256" << endl; // Max value of a pixel RGB value ...



  const double cr = -0.835;
  const double ci = -0.2321;

  double prevr, previ;

  for (unsigned int y = 0; y < height; ++y)
  {
    for (unsigned int x = 0; x < width; ++x)
    {
      double nextr = 1.5 * (2.0 * x / width - 1.0);
      double nexti = (2.0 * y / height - 1.0);

      for (unsigned int i = 0; i < maxIteration; ++i)
      {
        prevr = nextr;
        previ = nexti;

        nextr = prevr * prevr - previ * previ + cr;
        nexti = 2 * prevr * previ + ci;

        if (((nextr * nextr) + (nexti * nexti)) > 4)
        {
          int r = (i % brightnes);
          int g = ((i * 3) % brightnes);
          int b = (i % brightnes);

          // Output it to the image
          fout << r << " " << g << " " << b << " ";

          break;
        }
      }

    }
    fout << endl;
  }

  fout.close();
  cout << "Finished!" << endl;

}

void DataJuliaMono(unsigned char *data, double cr, double ci) {

  double prevr, previ;

  for (unsigned int y = 0; y < height; ++y)
  {
    for (unsigned int x = 0; x < width; ++x)
    {
      double nextr = 1.5 * (2.0 * x / width - 1.0);
      double nexti = (2.0 * y / height - 1.0);

      for (unsigned int i = 0; i < maxIteration; ++i)
      {
        prevr = nextr;
        previ = nexti;

        nextr = prevr * prevr - previ * previ + cr;
        nexti = 2 * prevr * previ + ci;

        if (i == maxIteration - 1) {
          *(data + (x + y * width) * 3 + 0) = 0;
          *(data + (x + y * width) * 3 + 1) = 0;
          *(data + (x + y * width) * 3 + 2) = 0;
        }
        if (((nextr * nextr) + (nexti * nexti)) > 4)
        {
          int r = (i % brightnes);
          int g = ((i * 3) % brightnes);
          int b = (i % brightnes);

          // Output it to the data
          *(data + (x + y * width) * 3 + 0) = r;
          *(data + (x + y * width) * 3 + 1) = g;
          *(data + (x + y * width) * 3 + 2) = b;

          break;
        }
      }

    }
  }

}
/*
void MultiThread() {
  const unsigned int maxThreads = 1;

  std::thread threads[maxThreads];

  for (int i = 0; i < maxThreads; i++) {
    threads[i] = std::thread(threadsIteration);
  }

  for (int i = 0; i < maxThreads; i++) {
    threads[i].join();
  }

}*/


void DataJuliaSchd(unsigned char *data, double cr, double ci) {

  for (int first_y = 0; first_y < 8; first_y++) {

    auto FractalChunk = [data, cr, ci, first_y] {
      for (unsigned int y = first_y * (height / 8); y < (first_y + 1)*(height / 8); ++y)
      {
        double prevr, previ;
        for (unsigned int x = 0; x < width; ++x)
        {

          double nextr = 1.5 * (2.0 * x / width - 1.0);
          double nexti = (2.0 * y / height - 1.0);
          for (unsigned int i = 0; i < maxIteration; ++i)
          {
            prevr = nextr;
            previ = nexti;

            nextr = prevr * prevr - previ * previ + cr;
            nexti = 2 * prevr * previ + ci;

            if (i == maxIteration - 1) {
              *(data + (x + y * width) * 3 + 0) = 0;
              *(data + (x + y * width) * 3 + 1) = 0;
              *(data + (x + y * width) * 3 + 2) = 0;
            }
            if (((nextr * nextr) + (nexti * nexti)) > 4)
            {
              int r = (i % brightnes);
              int g = ((i * 3) % brightnes);
              int b = (i % brightnes);

              // Output it to the data
              *(data + (x + y * width) * 3 + 0) = r;
              *(data + (x + y * width) * 3 + 1) = g;
              *(data + (x + y * width) * 3 + 2) = b;

              break;
            }
          }
        }
      }
    };
    schd.run(FractalChunk, &s1);
  }
  schd.waitFor(s1);
}


void threadsIteration() {

  std::vector<std::string> vector_test;
  int pereza = 10000;

  vector_test.reserve(pereza);

  char buffer[20];

  for (int i = 0; i < pereza; i++)
  {
    vector_test.push_back(_itoa(i, buffer, 10));

  }

}

void threadIteration() {

  std::vector<std::string> vector_test;

  vector_test.reserve(80000);

  char buffer[20];

  for (int i = 0; i < 80000; i++)
  {
    vector_test.push_back(_itoa(i, buffer, 10));

  }

}

void MonoThread() {

  threadIteration();

  //printf("MonoThread: %f\n\n", clockMonothread.ellapsedMS());
}
/*
void MultiThread() {
  const unsigned int maxThreads = 8;

  std::thread threads[maxThreads];

  for (int i = 0; i < maxThreads; i++) {
    threads[i] = std::thread(threadsIteration);
  }

  for (int i = 0; i < maxThreads; i++) {
    threads[i].join();
  }

}*/

void Scheduler() {

  px_sched::Scheduler schd;
  px_sched::SchedulerParams schdParams;

  int esteMen = 8;

  schdParams.num_threads = esteMen;
  schdParams.max_running_threads = esteMen;
  schd.init(schdParams);

  px_sched::Sync s;

  for (size_t i = 0; i < esteMen; ++i) {
    auto job = [] {
      threadsIteration();
    };
    // run a task, and notify to the given sync object
    schd.run(job, &s);
  }

  //printf("Waiting for tasks to finish...\n");
  schd.waitFor(s); // wait for all tasks to finish
                   //printf("Waiting for tasks to finish...DONE \n");


}


void prepare_quad() {

  float positions[12] = {
    -1.0f, -1.0f, 0.5f,
     1.0f, -1.0f, 0.5f,
     1.0f,  1.0f, 0.5f,
    -1.0f,  1.0f, 0.5f,
  };

  float uvs[8] = {
    0.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 1.0f,
    0.0f, 1.0f
  };

  glGenBuffers(1, &quad_vertices_ID);
  glBindBuffer(GL_ARRAY_BUFFER, quad_vertices_ID);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 20, positions, GL_STATIC_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, sizeof(float) * 12, sizeof(float) * 8, uvs);

  unsigned short indices[6] = {
    0,1,2,
    2,3,0
  };

  glGenBuffers(1, &quad_indices_ID);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quad_indices_ID);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * 6, indices, GL_STATIC_DRAW);


  quad_vertex_shader_ID = glCreateShader(GL_VERTEX_SHADER);
  quad_fragment_shader_ID = glCreateShader(GL_FRAGMENT_SHADER);

  char *vertex_source[] = {
    "#version 330 core \n",
    "layout (location = 0) in vec3 a_pos;\n",
    "layout (location = 1) in vec2 a_uv;\n",
    "out vec2 uv;\n",
    "void main(){\n",
    "  uv = a_uv;\n"
    "  gl_Position = vec4(a_pos, 1.0);\n",
    "}"
  };

  char *fragment_source[] = {
    "#version 330 core \n",
    "out vec4 FragColor;\n",
    "in vec2 uv;\n",
    "uniform sampler2D tex;\n",
    "void main(){\n",
    "  //FragColor = vec4(uv.x, uv.y, 0, 1.0);\n",
    "  FragColor = texture(tex, uv);\n",
    "}"
  };

  char log[512];

  glShaderSource(quad_vertex_shader_ID, 7, vertex_source, 0);
  glCompileShader(quad_vertex_shader_ID);
  glGetShaderInfoLog(quad_vertex_shader_ID, 512, 0, log);
  printf("%s\n", log);

  glShaderSource(quad_fragment_shader_ID, 8, fragment_source, 0);
  glCompileShader(quad_fragment_shader_ID);
  glGetShaderInfoLog(quad_fragment_shader_ID, 512, 0, log);
  printf("%s\n", log);


  quad_program_ID = glCreateProgram();
  glAttachShader(quad_program_ID, quad_vertex_shader_ID);
  glAttachShader(quad_program_ID, quad_fragment_shader_ID);
  glLinkProgram(quad_program_ID);

  glDeleteShader(quad_vertex_shader_ID);
  glDeleteShader(quad_fragment_shader_ID);

}

void render_quad() {

  glBindBuffer(GL_ARRAY_BUFFER, quad_vertices_ID);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);

  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void *)(sizeof(float) * 12));
  glEnableVertexAttribArray(1);

  glUseProgram(quad_program_ID);

  glBindTexture(GL_TEXTURE_2D, texture);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quad_indices_ID);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
}

void render_function() {
  int mask = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
  glClear(mask);

  render_quad();
}

void prepare(unsigned char *tex_data){

  prepare_quad();

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  // set the texture wrapping/filtering options (on the currently bound texture object)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, tex_data);
  glGenerateMipmap(GL_TEXTURE_2D);

  glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
}


int main() {
  

  int current_frame = 0;
  double *time_values = (double *)malloc(sizeof(double) * 100);
  RZE::Clock clock;

  RZE::Window mainWindow;
  mainWindow.initWindow(256, 256);

  px_sched::SchedulerParams schd_params;
  schd_params.num_threads = 8;
  schd_params.max_running_threads = 8;

  schd.init(schd_params);

  unsigned char *fractal_data = nullptr;
  // Allocating data for pixel
  fractal_data = (unsigned char *)malloc(sizeof(unsigned char) * width * height * 3);
  prepare(fractal_data);

  double c_real = -0.8;
  double c_imaginary = 0.156; 

  bool reverse = false;
  bool close_window = false;
  

  while (mainWindow.processEvents() && !close_window) {
    clock.start();
	  DataJuliaSchd(fractal_data, c_real, c_imaginary);
    clock.stop();
    if(current_frame < 100) time_values[current_frame] = clock.ellapsedMS();
	  if (current_frame == 99) {
		  double final_value = 0.0;
		  double greatest_value = 0.0;
		  double smallest_value = 100.0;

		  for (int i = 0; i < 100; ++i) {
			  final_value += time_values[i];
			  if (time_values[i] > greatest_value) greatest_value = time_values[i];
			  if (time_values[i] < smallest_value) smallest_value = time_values[i];
		  }
		
		  printf("\n The median time value was %f\nThe greatest time was %f\nAnd the smallest was %f\n", final_value / 100.0, greatest_value, smallest_value);
		  close_window = true;
	  }
	//if (c_imaginary > 0.5) reverse = true;
	//if (c_imaginary < -0.5) reverse = false;

	//c_imaginary += reverse ?  -0.01f : 0.01;
	current_frame++;

    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, fractal_data);

    render_function();
    mainWindow.swap();
  }
  
  mainWindow.shutdownWindow();

  system("pause");

  return 0;
}