#define PX_SCHED_IMPLEMENTATION 1
#include "deps/PX/px_sched.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <stdio.h>
#include <conio.h>
#include <chrono>
#include <vector>
#include <string>
#include <iostream>
#include <time.h>
#include "common_errors.h"

#include "obj_loader.h"

#include "platform_types.h"
#include "render_context.h"
#include "window.h"
#include "glfw3.h"
#include "imgui_window.h"
#include "clock.h"
#include "display_list.h"
#include "camera.h"
#include "geo_primitives.h"
#include <gtc/matrix_transform.hpp>
#include "minitrace/minitrace.h"
#include "Lua/lua.hpp"
#include "node.h"




float32 accumTime;

RZE::RenderContext *render_ctx;

RZE::Camera camera;

px_sched::Scheduler schd;
px_sched::Sync logic_sync;
RZE::ImguiWindow imguiWindow;

bool lua_button_alive = false;

float clear_color[4] = { 0.5f,0.5f, 0.5f, 1.0f };

int32 screen_mask = RZE::ClearBit::kClearBit_Color | RZE::ClearBit::kClearBit_Depth;

std::string output = "";

static int LoadLuaGeo(lua_State *L) {

	glm::vec3 pos = { 0.0f, 0.0f, 0.0f };
	glm::vec3 scale = { 0.0f, 0.0f, 0.0f };
	glm::vec3 rot = { 0.0f, 0.0f, 0.0f };

	float32 angle = 0;

	//TODO Mirarlo ya que va al reves,restar i
	for (int i = 2; i >= 0; i--) {
		pos[i] = lua_tonumber(L, -8);
		lua_pop(L, -8);

	//	/*pos[i] = lua_tonumber(L, -1);
	//	lua_pop(L, -1);

	//	pos[i] = lua_tonumber(L, -1);
	//	lua_pop(L, -1);*/
	}
	
  RZE::RenderContext *ctx;
  RZE::DisplayList *dl;


	lua_pushnumber(L, 0);
	lua_gettable(L, LUA_REGISTRYINDEX);
  ctx = (RZE::RenderContext*)lua_touserdata(L, -1);

  lua_pushnumber(L, 1);
  lua_gettable(L, LUA_REGISTRYINDEX);
  dl = (RZE::DisplayList*)lua_touserdata(L, -1);

  RZE::Node parent_node = RZE::Node::parent_node(ctx);
  RZE::Node cubito = parent_node.create_child();

  cubito.set_position(pos);
  cubito.set_rotation(glm::vec3(0.0f, 1.0f, 0.0f), 0.0f);
  cubito.set_scale(glm::vec3(1.0f));
	
	RZE::RenderObjectIndexes cubito_indices = RZE::GeoPrimitives::RenderCube(ctx, dl, RZE::kShaderType::kShaderTypeNormals, glm::vec3(pos.x, pos.y, pos.z), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f);
  cubito.set_geo_indexes(cubito_indices);

	return 1;
}

void LoadLuaFile(lua_State *L, RZE::RenderContext *ctx, RZE::DisplayList *logic_dl) {
	
	if (!L) {
		lua_close(L);
	}


	RZE::DisplayList *dl_tmp = logic_dl;

	lua_pushnumber(L, 0);
	lua_pushlightuserdata(L, ctx);
	lua_settable(L, LUA_REGISTRYINDEX);

  lua_pushnumber(L, 1);
  lua_pushlightuserdata(L, dl_tmp);
  lua_settable(L, LUA_REGISTRYINDEX);


	lua_pushcfunction(L, LoadLuaGeo);
	lua_setglobal(L, "LoadLuaGeo");

	
	if (luaL_dofile(L, "../resources/scripting/generate_geoC.lua")) {
		output = lua_tostring(L, -1);
		lua_pop(L, -1);
		printf("%s\n", output.c_str());

	}
	
 	//State_.push_back(RZE::GeoPrimitives::RenderCube(render_ctx, dl, glm::vec3(pos.x, pos.y, pos.z), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f));

	

	lua_button_alive = false;
}

void logic(RZE::Window *mainWindow) {
	

	int all_logic;

	lua_State *L = luaL_newstate();
	luaL_openlibs(L);

	RZE::DisplayList *logic_dl = new RZE::DisplayList();


	logic_dl->setRenderContext(render_ctx);

  RZE::Node parent_node = RZE::Node::parent_node(render_ctx);
  RZE::Node sun = parent_node.create_child();


  sun.set_position(0.0f, 0.0f, 0.0f);
  sun.set_rotation(0.0f, 1.0f, 0.0f, 0.0f);
  sun.set_scale(0.1f, 0.1f, 0.1f);

  //Should be internal to the pre-loading of shaders
  logic_dl->createMaterial(render_ctx->material_ids_[0], 0, 0);
  logic_dl->createMaterial(render_ctx->material_ids_[1], 0, 1);
  logic_dl->createMaterial(render_ctx->material_ids_[2], 0, 2);

  logic_dl->createTexture("../resources/textures/Mario_block.png", 0, RZE::Command_createTexture::T_2D, RZE::Command_createTexture::P_MIN_FILTER, RZE::Command_createTexture::W_REPEAT, RZE::Command_createTexture::F_RGBA, RZE::Command_createTexture::FL_LINEAR);

	std::vector<RZE::RenderObjectIndexes> obj;
	obj = RZE::ObjLoader::loadObjRender(render_ctx, logic_dl, RZE::kShaderType::kShaderTypeNormals, glm::identity<glm::mat4>(), (char8*)"../bin/obj/man.obj");

  RZE::RenderObjectIndexes pyramid = RZE::GeoPrimitives::RenderPiramid(render_ctx, logic_dl, RZE::kShaderType::kShaderTypeRed, glm::vec3(0.0f, 0.0f, 5.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f,1.0f,0.0f), 0.0f);
  RZE::RenderObjectIndexes cube = RZE::GeoPrimitives::RenderCube(render_ctx, logic_dl, RZE::kShaderType::kShaderTypeUvs, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f);
  
  sun.set_geo_indexes(cube);
  

	camera.changeSpeed(2.0f, 50.0f, 0.10f);

  float rot = 0.0f;

  

	while (mainWindow->processEvents()) {

		logic_dl->clearWindow(clear_color, screen_mask);

		camera.mouseInput(mainWindow, false);
		camera.keyboardInput(mainWindow);
		camera.logic(0.1);

    if (mainWindow->getKeyDown(RZE::InputKey::KINPUT_KEY_F1)) {
      render_ctx->internal_nodes().geo_indexes_[1].material_id = static_cast<uint32>(RZE::kShaderType::kShaderTypeRed);
    }
    if (mainWindow->getKeyDown(RZE::InputKey::KINPUT_KEY_F2)) {
      render_ctx->internal_nodes().geo_indexes_[1].material_id = static_cast<uint32>(RZE::kShaderType::kShaderTypeNormals);
    }
    if (mainWindow->getKeyDown(RZE::InputKey::KINPUT_KEY_F3)) {
      render_ctx->internal_nodes().geo_indexes_[1].material_id = static_cast<uint32>(RZE::kShaderType::kShaderTypeUvs);
    }

		/*for (int i = 0; i < State_.size(); ++i) {
			logic_dl->render(State_[i].material_id, State_[i].vertex_id, State_[i].index_id);
		}*/
    
    parent_node.set_rotation(0.0f, 1.0f, 0.0f, rot * 8);
   

    render_ctx->CalculateLocalModels();
    render_ctx->CalculateNestedModels();
    render_ctx->RenderNodes(logic_dl);

		if (lua_button_alive) {
			LoadLuaFile(L, render_ctx, logic_dl);
		}

		render_ctx->copyDisplayList(logic_dl);
	
		logic_dl->clearVector();
	

    rot += 0.01f;
	}

}





int main() {

	
	spdlog::init_thread_pool(8192, 1);
	spdlog::flush_every(std::chrono::seconds(5));
	spdlog::get("console") = spdlog::create_async_nb<spdlog::sinks::stdout_color_sink_mt>("console");

	spdlog::get("console")->info("Asyncronous console logger ready");

	
	RZE::Window mainWindow;
	mainWindow.initWindow(1000, 1000);

	
	render_ctx = new RZE::RenderContext();

	// Global level of message visibility
	// trace displays debug, while info doesn't
	spdlog::set_level(spdlog::level::trace);

	px_sched::SchedulerParams schd_params;
	schd_params.num_threads = 8;
	schd_params.max_running_threads = 8;
	schd.init(schd_params);
	

	srand(time(NULL));


	
	auto logic_job = [&mainWindow] { logic(&mainWindow); };
	schd.run(logic_job, &logic_sync);
	

	//glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);


	while (mainWindow.processEvents()) {
		float delta_time = (float)glfwGetTime() - accumTime;
		accumTime = (float)glfwGetTime();

		ImGui_ImplGlfwGL3_NewFrame();

		

		imguiWindow.imguiMovableWindowInit(ImVec2(700, 200), "LUA TEST");
		imguiWindow.imguiTextStyle("Press the button to refresh the LUA file");

		imguiWindow.imguiButtonStyle();

		if (ImGui::Button("LOAD")) {
			
			lua_button_alive = true;
		}

		ImGui::PopStyleColor(3);

		ImGui::Text(output.c_str());

		imguiWindow.imguiWindowShutdown();

		
		render_ctx->updateViewMatrix(camera.getViewProjectionMatrix(70.0f, 1280.0f, 1280.0f, 0.1f, 100.0f));
		

		
		mainWindow.InputService(); // Before Logic starts
		

		render_ctx->render();
		ImGui::Render();
		mainWindow.swap();
	


	}

	//Preventing bug with async log and VS
	spdlog::shutdown();

	mainWindow.shutdownWindow();

	return 0;
}



//#define PX_SCHED_IMPLEMENTATION 1
//#include "app.h"
//
//int main() {
//
//	RZE::App *app = new RZE::App();
//
//	app->init();
//
//	app->start();
//
//	app->shutDown();
//}