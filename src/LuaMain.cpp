#include "Lua/lua.hpp"
#include <stdlib.h>

int main() {
	
	lua_State *L = luaL_newstate();
	luaL_dostring(L, "x = 42");
	lua_getglobal(L, "x");
	lua_Number x = lua_tonumber(L, -1);
	printf("lua syas x = %d\n", (int)x);
	lua_close(L);

	system("pause");
}