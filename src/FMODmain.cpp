
//#include "SoLoud\soloud_wav.h"
//#include <string.h>
//#include <stdio.h>
//#include <stdlib.h>
//
//#define DR_WAV_IMPLEMENTATION
//#include "..\SoLoud\audiosource\wav\dr_wav.h"
//#include "SoLoud\soloud_wav.h"

#include "audio_loader.h"

#include "deps/FMOD/lowlevel/fmod.hpp"
#include "window.h"
#include "imgui_window.h"
#include "audio_system.h"
#include "common.h"
#include "render_context.h"
#include "display_list.h"
#include "platform_types.h"
#include "common_errors.h"
#include "audio_spectrum_visualizer.h"



RZE::RenderContext *render_ctx;

float clear_color[4] = { 0.5f,0.5f, 0.5f, 1.0f };

int32 screen_mask = RZE::ClearBit::kClearBit_Color | RZE::ClearBit::kClearBit_Depth;

void chargeSounds(RZE::AudioSystem *audioSystem, std::vector<RZE::Layering>*layer_vec_) {

  audioSystem->chargeSound(&audioSystem->Tracks[0], "village_theme.flac", true);
  audioSystem->chargeSound(&audioSystem->Tracks[1], "village_theme.flac", true);
	audioSystem->chargeSound(&audioSystem->Tracks[1], "test.flac", true);
	audioSystem->chargeSound(&audioSystem->Tracks[2], "guitar.flac", false);
	audioSystem->chargeSound(&audioSystem->Tracks[3], "bass.flac", false);
	audioSystem->chargeSound(&audioSystem->Tracks[4], "synth.flac", false);
	audioSystem->chargeSound(&audioSystem->Tracks[5], "Branching_A_0.flac", true);
	audioSystem->chargeSound(&audioSystem->Tracks[6], "Branching_B_0.flac", true);
	audioSystem->chargeSound(&audioSystem->Tracks[7], "Branching_B_1.flac", true);
	audioSystem->chargeSound(&audioSystem->Tracks[8], "Branching_Transition.flac", true);

	RZE::Layering layer_1;
	layer_1.init(&audioSystem->Tracks[2], 0.0f);
	layer_vec_->push_back(layer_1);

	RZE::Layering layer_2;
	layer_2.init(&audioSystem->Tracks[3], 7.4f);
	layer_vec_->push_back(layer_2);

	RZE::Layering layer_3;
	layer_3.init(&audioSystem->Tracks[4], 14.8f);
	layer_vec_->push_back(layer_3);

}


int main(int argc, char** argv)
{
	RZE::Window mainWindow;
	mainWindow.initWindow(1920, 1080);

	spdlog::init_thread_pool(8192, 1);
	spdlog::flush_every(std::chrono::seconds(5));
	spdlog::get("console") = spdlog::create_async_nb<spdlog::sinks::stdout_color_sink_mt>("console");

	spdlog::get("console")->info("Asyncronous console logger ready");

	render_ctx = new RZE::RenderContext();

	RZE::DisplayList *dl = new RZE::DisplayList();
	RZE::ImguiWindow imguiWindow;
	RZE::AudioSystem* audioSystem = new RZE::AudioSystem();
	std::vector<RZE::Layering> *layer_vec_ = new std::vector<RZE::Layering>;

	dl->setRenderContext(render_ctx);

	chargeSounds(audioSystem, layer_vec_);

  RZE::RGBA_color back = { 50, 50, 50, 255};
  RZE::RGBA_color graph = { 255, 255, 255, 255};

  RZE::AudioSpectrumVisualizer::GenerateSpectrumImage("test.flac", audioSystem->AudioLoader, back, graph);


	while (mainWindow.processEvents()) {

		dl->clearWindow(clear_color, screen_mask);

		ImGui_ImplGlfwGL3_NewFrame();

		mainWindow.InputService();

		imguiWindow.imguiMovableWindowInit(ImVec2(500, 500), "Audio Techniques");
		imguiWindow.imguiTextStyle("Choose the technique to heard ");

		audioSystem->update();

		///////// CROSSFADING /////////
		ImGui::Spacing();
		if (ImGui::TreeNode("Crossfading"))
		{
			if (imguiWindow.buttons[0].button_active_) {
				imguiWindow.imguiButtonStyle();
			}
			else {

				imguiWindow.imguiButtonPlayStyle();
			}

			if (ImGui::Button("PLAY") && imguiWindow.buttons[0].button_active_) {

				if (imguiWindow.buttons[0].play_active_) {
					audioSystem->playSound(&audioSystem->Tracks[0], 1.0f);
					audioSystem->playSound(&audioSystem->Tracks[1], 0.2f);
					audioSystem->stopChannel(&audioSystem->Tracks[1]);
				}


				if (!imguiWindow.buttons[0].play_active_) {
					audioSystem->playSound(&audioSystem->Tracks[0], 1.0f);
					audioSystem->playSound(&audioSystem->Tracks[1], 0.2f);
					audioSystem->stopChannel(&audioSystem->Tracks[1]);
				}

				imguiWindow.buttons[0].play_active_ = false;
				imguiWindow.buttons[0].button_active_ = false;
				imguiWindow.buttons[0].cross_active_ = false;
			}
			ImGui::PopStyleColor(3);

			if (!imguiWindow.buttons[0].button_active_) {
				imguiWindow.imguiButtonStyle();
			}
			else {

				imguiWindow.imguiButtonStopStyle();
			}

			ImGui::SameLine();
			if (ImGui::Button("STOP") && !imguiWindow.buttons[0].button_active_) {

				audioSystem->setChannelVolume(&audioSystem->Tracks[0], 1.0f);
				audioSystem->stopChannel(&audioSystem->Tracks[0]);
				audioSystem->stopChannel(&audioSystem->Tracks[1]);
				imguiWindow.buttons[0].button_active_ = true;
			}
			ImGui::PopStyleColor(3);

			imguiWindow.imguiButtonStyle();

			ImGui::SameLine();
			if (ImGui::Button("CROSSFADING") && !imguiWindow.buttons[0].cross_active_) {
				audioSystem->playChannel(&audioSystem->Tracks[1]);
				audioSystem->crossfading(&audioSystem->Tracks[0], (&audioSystem->Tracks[1]), 5, 20, 3);
				imguiWindow.buttons[0].cross_active_ = true;
			}

			ImGui::PopStyleColor(3);
			ImGui::TreePop();
		}
		////////////////////////////////

		///////// LAYERING /////////
		ImGui::Spacing();
		if (ImGui::TreeNode("Layering"))
		{
			if (imguiWindow.buttons[1].button_active_) {
				imguiWindow.imguiButtonStyle();
			}
			else {

				imguiWindow.imguiButtonPlayStyle();
			}

			if (ImGui::Button("PLAY") && imguiWindow.buttons[1].button_active_) {


				audioSystem->layering(layer_vec_);

				imguiWindow.buttons[1].button_active_ = false;
			}

			ImGui::PopStyleColor(3);

			if (!imguiWindow.buttons[1].button_active_) {
				imguiWindow.imguiButtonStyle();
			}
			else {

				imguiWindow.imguiButtonStopStyle();
			}

			ImGui::SameLine();
			if (ImGui::Button("STOP") && !imguiWindow.buttons[1].button_active_) {
				audioSystem->stopChannel(&audioSystem->Tracks[2]);
				audioSystem->stopChannel(&audioSystem->Tracks[3]);
				audioSystem->stopChannel(&audioSystem->Tracks[4]);
				imguiWindow.buttons[1].button_active_ = true;
			}

			ImGui::PopStyleColor(3);
			ImGui::TreePop();
		}
		////////////////////////////////

		///////// BRANCHING /////////
		ImGui::Spacing();
		if (ImGui::TreeNode("Branching"))
		{
			ImGui::Spacing();
			imguiWindow.imguiTextStyle("You are a spy who is in an enemy base,press the different \nbuttons to trigger the diferent events to change the music.");

			ImGui::Spacing();
			ImGui::Spacing();
			ImGui::Spacing();
			imguiWindow.imguiTextStyle("You are hiding");

			if (imguiWindow.buttons[2].button_active_) {
				imguiWindow.imguiButtonStyle();
			}
			else {

				imguiWindow.imguiButtonPlayStyle();
			}

			if (ImGui::Button("PLAY") && imguiWindow.buttons[2].button_active_) {


				audioSystem->branching(&audioSystem->Tracks[5], &audioSystem->Tracks[8]);


				imguiWindow.buttons[2].button_active_ = false;
				imguiWindow.buttons[3].button_active_ = false;
			}
			ImGui::PopStyleColor(3);

			if (imguiWindow.buttons[2].button_active_ &&
				imguiWindow.buttons[3].button_active_ &&
				imguiWindow.buttons[4].button_active_) {

				imguiWindow.imguiButtonStopStyle();
			}
			else {

				imguiWindow.imguiButtonStyle();
			}

			ImGui::SameLine();
			if (ImGui::Button("STOP")) {
				audioSystem->stopBranching();

				imguiWindow.buttons[2].button_active_ = true;
				imguiWindow.buttons[3].button_active_ = true;
				imguiWindow.buttons[4].button_active_ = true;
			}

			ImGui::PopStyleColor(3);

			ImGui::Spacing();
			ImGui::Spacing();
			ImGui::Spacing();
			imguiWindow.imguiTextStyle("You are in the aggro of the enemy");
			ImGui::PushID("Play2");

			if (imguiWindow.buttons[3].button_active_ && imguiWindow.buttons[2].button_active_ &&
				!imguiWindow.buttons[4].button_active_) {


				imguiWindow.imguiButtonPlayStyle();
			}
			else {

				imguiWindow.imguiButtonStyle();

			}

			if (ImGui::Button("PLAY") && !imguiWindow.buttons[3].button_active_) {


				audioSystem->branching(&audioSystem->Tracks[6], &audioSystem->Tracks[8]);


				imguiWindow.buttons[3].button_active_ = true;
				imguiWindow.buttons[4].button_active_ = false;
				imguiWindow.buttons[2].button_active_ = true;
			}
			ImGui::PopStyleColor(3);
			ImGui::PopID();

			ImGui::Spacing();
			ImGui::Spacing();
			ImGui::Spacing();
			imguiWindow.imguiTextStyle("The enemy finds you");
			ImGui::PushID("Play3");

			if (imguiWindow.buttons[4].button_active_ &&
				!imguiWindow.buttons[3].button_active_ &&
				imguiWindow.buttons[2].button_active_) {

				imguiWindow.imguiButtonPlayStyle();
			}
			else {

				imguiWindow.imguiButtonStyle();
			}
			if (ImGui::Button("PLAY") && !imguiWindow.buttons[4].button_active_) {

				audioSystem->branching(&audioSystem->Tracks[7], &audioSystem->Tracks[8]);

				imguiWindow.buttons[3].button_active_ = false;
				imguiWindow.buttons[4].button_active_ = true;
			}
			ImGui::PopID();

			ImGui::PopStyleColor(3);
			ImGui::TreePop();
		}
		////////////////////////////////

		imguiWindow.imguiWindowShutdown();

		render_ctx->copyDisplayList(dl);

		dl->clearVector();

		render_ctx->render();

		ImGui::Render();

		mainWindow.swap();
	}

	mainWindow.shutdownWindow();
	return 0;
}