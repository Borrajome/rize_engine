

#include <stdlib.h>
#include <stdio.h>

#include "audio_system.h"



// Entry point
int main(int argc, char *argv[])
{
  RZE::SoLoudAudioSystem *audioSystem = new RZE::SoLoudAudioSystem();

  audioSystem->chargeSound(&audioSystem->Tracks[0], "village_theme.wav", true);
  audioSystem->playSound(&audioSystem->Tracks[0], 1.0f);
  system("pause");

  audioSystem->setChannelVolume(&audioSystem->Tracks[0], 0.5f);
  system("pause");

	audioSystem->stopAllChannel();

	audioSystem->chargeSound(&audioSystem->Tracks[1], "test.wav", true);

	audioSystem->playSound(&audioSystem->Tracks[0], 1.0f);
	audioSystem->playSound(&audioSystem->Tracks[1], 0.0f);
	audioSystem->crossfading(&audioSystem->Tracks[0], (&audioSystem->Tracks[1]), 5, 20, 3);
  system("pause");

	audioSystem->stopAllChannel();

	audioSystem->chargeSound(&audioSystem->Tracks[2], "guitar.wav", false);
	audioSystem->chargeSound(&audioSystem->Tracks[3], "bass.wav", false);
	audioSystem->chargeSound(&audioSystem->Tracks[4], "synth.wav", false);

	audioSystem->playSound(&audioSystem->Tracks[2], 1.0f);
	audioSystem->playSound(&audioSystem->Tracks[3], 0.8f);
	audioSystem->playSound(&audioSystem->Tracks[4], 1.0f);

	audioSystem->layering(&audioSystem->Tracks[3], &audioSystem->Tracks[4]);


	system("pause");
	// All done.

  audioSystem->shutdown();
	return 0;
}